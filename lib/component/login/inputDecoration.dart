import 'package:flutter/material.dart';

class DecorationComponent {
  static InputDecoration inputDecoration(BuildContext context, {String? hintText}) {
    return InputDecoration(
      hintText: hintText,
      labelStyle: TextStyle(color: Theme.of(context).primaryColor),
      contentPadding: EdgeInsets.all(24),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xFFE0E3E7),
          width: 2,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xAA003478),
          width: 2,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      filled: true,
      fillColor: Colors.white,
      border: OutlineInputBorder(),
    );
  }
}

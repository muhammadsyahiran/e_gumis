import 'package:flutter/material.dart';

class ErrorText extends StatelessWidget {
  final String message;

  const ErrorText({required this.message});

  @override
  Widget build(BuildContext context) {
    return message.isNotEmpty
        ? Text(
            message,
            style: TextStyle(color: Colors.red),
          )
        : SizedBox.shrink();
  }
}


import 'package:flutter/material.dart';

class SuccessfullPopup extends StatelessWidget {
  final String title;
  final String subTitle;
  final VoidCallback? onTap;

  const SuccessfullPopup({required this.title, this.subTitle = '', this.onTap});

  static const textStyle = TextStyle(fontSize: 21, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromARGB(13, 158, 158, 158),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Stack(
              alignment: Alignment.topCenter,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 200, left: 20, right: 20),
                  child: GestureDetector(
                    onTap: onTap,
                    child: Card(
                      child: Icon(Icons.close, color: Colors.grey),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 165, left: 20, right: 20),
                  child: Container(
                    height: 30,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.green),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 27),
                  child: Card(
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(30),
                        child: Container(
                          height: 90,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: Text(
                                  title,
                                  style: TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                              ),
                              // SizedBox(height: 20),
                              if (subTitle.isNotEmpty) ...[
                                SizedBox(height: 10),
                                Flexible(
                                  child: Text(
                                    subTitle,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.grey),
                                  ),
                                ),
                              ],
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.green,
                  child: Icon(Icons.check, color: Colors.white, size: 40),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

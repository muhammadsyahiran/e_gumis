import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateTimeComponent extends StatelessWidget {
  const DateTimeComponent({super.key});

  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now().toUtc().add(Duration(hours: 8));
    String formattedDate = DateFormat('d MMMM yyyy, hh:mm a', 'ms').format(now);
    return Text(formattedDate);
  }
}

import 'package:flutter/material.dart';
import '../../widget/vers.dart';

class StaticBanner extends StatelessWidget {
  const StaticBanner({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFEFF6FF), // Light blue background color
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5), // Adjust padding
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: VersionNumber(),
          ),
        ],
      ),
    );
  }
}

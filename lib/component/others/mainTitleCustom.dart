import 'package:flutter/material.dart';

class MainTitleCustom extends StatelessWidget {
  final String title;

  MainTitleCustom({required this.title});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.fromSTEB(24, 10, 0, 10),
      // padding: EdgeInsetsDirectional.fromSTEB(24, 10, 0, 10),
      // padding: const EdgeInsets.only(bottom: 10, left: 20),
      child: Text(
        title,
        style: TextStyle(
          color: Theme.of(context).primaryColor,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

import 'package:e_gumis/I10n/localizations.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

class ContactUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Card(
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                children: [
                  Text(
                    context.translate('contactUs'),
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                      'Pendaftar Wang Tak Dituntut Jabatan Akauntan Negara Malaysia (Accountant General’s Department of Malaysia) BAHAGIAN PENGURUSAN WANG TAK DITUNTUT',
                      style: TextStyle(fontWeight: FontWeight.w600)),
                  SizedBox(height: 10),
                  RowComponents(
                      text:
                          'ARAS 1, BLOK UTARA, PERBENDAHARAAN 2\nNO. 7, PERSIARAN PERDANA PRESINT 2\nKOMPLEKS KEMENTERIAN KEWANGAN\nPUSAT PENTADBIRAN KERAJAAN PERSEKUTUAN\n62594 PUTRAJAYA',
                      iconData: Icons.location_on,
                      launchingItem:
                          "https://maps.app.goo.gl/TXuVVYQVWQhzk5Vb9"),
                  SizedBox(height: 20),
                  RowComponents(
                      text: '03-8000 8700',
                      iconData: Icons.call,
                      launchingItem: "tel://0380008700"),
                  SizedBox(height: 20),
                  RowComponents(
                      text: '03-8000 8600',
                      iconData: Icons.call,
                      launchingItem: "tel://0380008600"),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class RowComponents extends StatelessWidget {
  final String text;
  final String launchingItem;
  final IconData iconData;

  const RowComponents(
      {required this.text,
      required this.launchingItem,
      required this.iconData});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Flexible(child: Text(text)),
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(20),
          ),
          child: IconButton(
            icon: Icon(
              iconData,
              color: Colors.white,
              size: 24,
            ),
            onPressed: () {
              launchUrlString(launchingItem);
            },
          ),
        ),
      ],
    );
  }
}

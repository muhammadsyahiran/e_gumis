import 'dart:async';
import 'dart:convert';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:e_gumis/screen/announcementListScreen.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_html/flutter_html.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../../screen/announcementDetailsScreen.dart';
import '../../widget/clickTrackingState.dart';
import '../config.dart';
import 'loadingIndicator.dart';
import 'mainTitleCustom.dart';

class AnnouncementComponent extends StatefulWidget {
  @override
  State<AnnouncementComponent> createState() => _AnnouncementComponentState();
}

class _AnnouncementComponentState extends State<AnnouncementComponent> {
  List<dynamic> data = [];
  Map<int, String> announcementDetails = {}; // Cache for fetched announcementDetail data
  PageController _pageController = PageController();
  int _currentPage = 0;
  Timer? _timer;
  bool _loading = true;

  @override
  void initState() {
    super.initState();
    fetchData();
    _startAutoSlide();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _timer?.cancel();
    super.dispose();
  }

  void _startAutoSlide() {
    _timer = Timer.periodic(Duration(seconds: 3), (Timer timer) {
      setState(() {
        _currentPage = (_currentPage + 1) % (data.isEmpty ? 1 : data.length);
        _pageController.animateToPage(
          _currentPage,
          duration: Duration(milliseconds: 300),
          curve: Curves.easeIn,
        );
      });
    });
  }

  Future<void> fetchData() async {
    try {
      final response = await http.get(Uri.parse('${Config.URL}/announcement'));
      if (response.statusCode == 200) {
        setState(() {
          Map<String, dynamic> jsonResponse = jsonDecode(response.body);
          List<dynamic> announcements = jsonResponse['announcements'];

          _loading = false;
          data = announcements.map((announcement) {
            if (announcement is Map<String, dynamic>) {
              return announcement;
            }
          }).toList();

          data = data.length > 4 ? data.sublist(0, 4) : data;
          _loading = false;
        });
      } else {
        throw Exception('Failed to load data');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  void onPageChanged(int page) {
    setState(() {
      _currentPage = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFEFF6FF),
      ),
      child: Padding(
        padding: EdgeInsetsDirectional.fromSTEB(0, 15, 0, 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MainTitleCustom(title: context.translate('announcement')),
                Align(
                  alignment: AlignmentDirectional(0, 1),
                  child: Padding(
                    padding: EdgeInsets.only(right: 24),
                    child: InkWell(
                      onTap: () {
                        //function for API report
                        trackClick('Paparan Senarai Pengumuman');
                        Navigator.push(context, MaterialPageRoute(builder: (context) => AnnouncementListScreen()));
                      },
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              context.translate('announcementList'),
                              style: TextStyle(color: Color(0xFFE7AC10), fontSize: 10, fontWeight: FontWeight.w600),
                            ),
                            SizedBox(width: 5),
                            Icon(Icons.list, color: Color(0xFFD2A015), size: 18),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 15),
            _loading
                ? Padding(
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                    child: Container(
                      height: 200,
                      child: Card(
                        child: Center(child: LoadingIndicator()),
                      ),
                    ),
                  )
                : Container(
                    height: 230,
                    child: PageView.builder(
                      controller: _pageController,
                      onPageChanged: onPageChanged,
                      itemCount: data.length,
                      itemBuilder: (context, index) {
                        String rawDate = data[index]['publishOn'].substring(0, 10); // Original date
                        String formattedDate = DateFormat('dd/MM/yyyy').format(DateTime.parse(rawDate)); // Format date

                        return CardAnnouncement(
                          paramId: data[index]['id'],
                          title: data[index][context.translate('titleUrl')],
                          date: formattedDate,
                          paragraph: data[index][context.translate('bodyUrl')],
                          announcementData: data,
                        );
                      },
                    ),
                  ),
            Align(
              alignment: AlignmentDirectional(0, 0),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                  data.length,
                  (index) => Container(
                    width: _currentPage == index ? 15 : 10,
                    height: _currentPage == index ? 15 : 10,
                    margin: EdgeInsets.symmetric(horizontal: 5),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _currentPage == index ? Color(0xFFF5BF03) : Colors.grey,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CardAnnouncement extends StatefulWidget {
  final String date;
  final String title;
  final String paragraph;
  final int paramId;
  final List<dynamic> announcementData;

  CardAnnouncement({
    required this.date,
    required this.title,
    required this.paragraph,
    required this.paramId,
    required this.announcementData,
  });

  @override
  _CardAnnouncementState createState() => _CardAnnouncementState();
}

class _CardAnnouncementState extends State<CardAnnouncement> {
  static Map<int, Map<String, dynamic>> _cache = {}; // Cache map to store fetched data
  late Future<Map<String, dynamic>> _announcementFuture;

  @override
  void initState() {
    super.initState();
    if (widget.paragraph.length < 5) {
      _announcementFuture = _getAnnouncementData(widget.paramId);
    } else {
      _announcementFuture = Future.value({});
    }
  }

  Future<Map<String, dynamic>> _getAnnouncementData(int paramId) async {
    // Check if the data is already cached
    if (_cache.containsKey(paramId)) {
      return Future.value(_cache[paramId]);
    }

    // If not cached, fetch the data from API
    final response = await http.get(Uri.parse('${Config.URL}/announcement/$paramId'));
    if (response.statusCode == 200) {
      Map<String, dynamic> announcementDetail = json.decode(response.body)['announcement'];
      // Cache the response for future use
      _cache[paramId] = announcementDetail;
      return announcementDetail;
    } else {
      throw Exception('Failed to load data');
    }
  }

  String extractBase64Image(String html) {
    final RegExp base64Pattern = RegExp(r'<img[^>]+src="data:image\/[^;]+;base64,([A-Za-z0-9+\/=]+)"');
    final match = base64Pattern.firstMatch(html);
    return match != null ? match.group(1) ?? '' : '';
  }

  Widget displayBase64Image(String base64String) {
    if (base64String.isNotEmpty) {
      final decodedImage = base64Decode(base64String);
      return Image.memory(decodedImage);
    }
    return SizedBox(); // Return an empty box if no base64 string found
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
      child: InkWell(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => AnnouncementDetailsScreen(paramId: widget.paramId)));
        },
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          widget.title.length > 15 ? '${widget.title.substring(0, 15)}...' : widget.title,
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.calendar_month,
                              color: Theme.of(context).primaryColor,
                              size: 13,
                            ),
                            const SizedBox(width: 4),
                            Text(
                              widget.date,
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 11,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    widget.paragraph.length < 5
                        ? FutureBuilder<Map<String, dynamic>>(
                            future: _announcementFuture,
                            builder: (context, snapshot) {
                              if (snapshot.connectionState == ConnectionState.waiting) {
                                return LoadingIndicator();
                              } else if (snapshot.hasError) {
                                return Text('Error: ${snapshot.error}');
                              } else if (snapshot.hasData) {
                                String announcementBody = snapshot.data?[context.translate('bodyUrl')] ?? '';
                                String base64Image = extractBase64Image(announcementBody);
                                return displayBase64Image(base64Image);
                              }
                              return SizedBox.shrink();
                            },
                          )
                        : Text(
                            widget.paragraph, 
                            maxLines: 7,
                            overflow: TextOverflow.ellipsis,
                          ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<Map<String, dynamic>> fetchDataAnnouncements(int paramId) async {
    final response = await http.get(Uri.parse('${Config.URL}/announcement/$paramId'));
    if (response.statusCode == 200) {
      Map<String, dynamic> announcementDetail = json.decode(response.body)['announcement'];
      return announcementDetail;
    } else {
      throw Exception('Failed to load data');
    }
  }
}

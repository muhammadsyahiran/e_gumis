import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';
// import 'package:flutter/services.dart';

class ReportFeedbackPopup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CardReportFeedbackPopup(
            icon: Icons.web,
            text: 'Aduan/Maklum balas Pengguna eGUMIS',
            onTap: () {
              Navigator.of(context).pop();
              launchUrlString("https://bit.ly/aduan-egumisjanm");
            },
          ),
          // CardReportFeedbackPopup(
          //   icon: Icons.web,
          //   text: 'Sistem Pengurusan Aduan Awam',
          //   onTap: () {
          //     Navigator.of(context).pop();
          //     launchUrlString(
          //         "https://anm.spab.gov.my/eApps/sdmscasepool/SdmsCasePool/add.do");
          //     // Navigator.pushReplacement(
          //     //   context,
          //     //   MaterialPageRoute(builder: (context) => WebViewApp()),
          //     // );
          //   },
          // ),
          // CardReportFeedbackPopup(
          //   icon: Icons.copy_all_rounded,
          //   text: 'tuntutan.bwtd@anm.gov.my',
          //   onTap: () {
          //     Navigator.of(context).pop();
          //     Clipboard.setData(
          //         ClipboardData(text: 'tuntutan.bwtd@anm.gov.my'));
          //     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          //       content: Text('E-mel berjaya disalin'),
          //       duration: Duration(seconds: 2),
          //       backgroundColor: Colors.green,
          //     ));
          //   },
          // ),
        ],
      ),
    );
  }
}

class CardReportFeedbackPopup extends StatelessWidget {
  final IconData icon;
  final String text;
  final VoidCallback onTap;

  const CardReportFeedbackPopup({
    required this.icon,
    required this.text,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(text),
            InkWell(
              onTap: onTap,
              child: Card(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Icon(
                    icon,
                    color: Theme.of(context).primaryColor,
                    size: 24,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// recheck all hardcode URL
// ========================
import 'dart:io';

import 'package:dart_ipify/dart_ipify.dart';

class Config {
  static const String domain = '10.23.27.83'; // run with domain ip server remote
  // static const String domain = '10.0.2.2'; // IP for run android emulator
  // static const String domain = 'localhost'; // run with google chorme and ios emulator
  // static const String domain = '192.168.68.100'; // IP for local device
  // static const String domain = 'egumis-dev.gov.my';

  static const String port = '8080';
  // static const String port = '3000/user';

  static const String URL = 'http://$domain:$port';
  // static const String URL = 'http://$domain';
}

class IpService {
  // Function to fetch IP details and return as a formatted string
  Future<String> getIpDetails() async {
    StringBuffer ipInfo = StringBuffer();

    try {
      for (var interface in await NetworkInterface.list()) {
        for (var addr in interface.addresses) {
          ipInfo.writeln(addr.address);
        }
      }
    } catch (e) {
      ipInfo.write('Failed to get IP details: $e');
    }

    return ipInfo.toString();
  }
}

Future<String> getPublicIpAddress() async {
  try {
    final ipAddress = await Ipify.ipv4(); // Get public IPv4 address
    return ipAddress;
  } catch (e) {
    return 'Failed to get IP address: $e';
  }
}

import 'dart:convert';
import 'package:e_gumis/component/others/loadingIndicator.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:e_gumis/component/config.dart';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:e_gumis/component/wtdListApproval/cardComponent.dart';

void showQueryMsgPopup(BuildContext context, rfdInfoId) async {
  const titleStyle = TextStyle(fontSize: 12, color: Color(0xffee8b60));

  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Center(
        child: LoadingIndicator(),
      );
    },
  );

  Map<String, dynamic>? refundInformation;

  await fetchData(context, rfdInfoId).then((data) {
    refundInformation = data;

    // Close the loading dialog
    Navigator.of(context).pop();

    // Show the actual data
    showDialog(
      context: context,
      builder: (BuildContext context) {
        _closeBtn() {
          Navigator.of(context).pop(true);
        }

        return Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                child: Padding(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text("Status", style: titleStyle),
                        Text(refundInformation?['rfdStatusNameMs'] ?? ''),
                        SizedBox(height: 10),
                        Text(context.translate('description'), style: titleStyle),
                        Text(refundInformation?['rfdStatusDescription'] ?? ''),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                _closeBtn();
                              },
                              child: ButtonComponent(
                                  textBtn: "OK",
                                  colorBtn: Theme.of(context).primaryColor),
                            ),
                          ],
                        )
                      ],
                    )),
              ),
            ],
          ),
        );
      },
    );
  });
}

Future<Map<String, dynamic>?> fetchData(BuildContext context, rfdInfoId) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final String? tokenValue = prefs.getString('tokenValue');

  final response = await http.get(Uri.parse('${Config.URL}/wtdStatusDetails/$rfdInfoId'),
    headers: {
      'Authorization': 'Bearer $tokenValue',
      'Content-Type': 'application/json',
    },
  );

  if (response.statusCode == 200) {
    Map<String, dynamic> responseApi = jsonDecode(response.body);
    return responseApi['refundInformation'] ?? {};
  } else {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text('Failed to load data')),
    );
    throw Exception('Failed to load data');
  }
}

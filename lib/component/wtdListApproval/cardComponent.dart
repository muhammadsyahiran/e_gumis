import 'package:e_gumis/component/config.dart';
import 'package:e_gumis/component/others/loadingIndicator.dart';
import 'package:e_gumis/component/wtdListApproval/popupMessage.dart';
import 'package:e_gumis/controller/pdfController.dart';
import 'package:e_gumis/screen/pdfViewScreen.dart';
import 'package:flutter/material.dart';
import '../../I10n/localizations.dart';
import '../../screen/wtdListApproveClaimInfoScreen.dart';
import 'package:path/path.dart' as path;

import '../../widget/clickTrackingState.dart';


class CardComponent extends StatelessWidget {
  final String dateTitle;
  final String dateSubTitle;
  final String refNoTitle;
  final String refNoSubTitle;
  final String actionTitle;
  final String status;
  final String amountTitle;
  final String amount;
  final int rfdInfoId;

  const CardComponent(
      {required this.dateTitle,
      required this.dateSubTitle,
      required this.refNoTitle,
      required this.refNoSubTitle,
      this.actionTitle = "",
      required this.status,
      required this.amountTitle,
      required this.amount,
      required this.rfdInfoId,});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(dateTitle, style: TextStyle(fontSize: 12)),
              Text(
                dateSubTitle,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).primaryColor),
              ),
              SizedBox(height: 10),
              Text(refNoTitle, style: TextStyle(fontSize: 12)),
              Text(
                refNoSubTitle,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).primaryColor),
              ),
              SizedBox(height: 10),
              Text(actionTitle, style: TextStyle(fontSize: 12)),
              Text(
                StatusUtil.getStatusAction(context, status),
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).primaryColor),
              ),
            ],
          ),
        ),
        SizedBox(height: 10),

        //Status Approval
        Padding(
          padding: EdgeInsets.only(left: 8),
          child: Card(
            color: getStatusColor(status),
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(19),
            ),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Text(
                StatusUtil.getStatusText(context, status),
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: getTextColor(status)),
              ),
            ),
          ),
        ),

        //Amount
        Padding(
          padding: EdgeInsets.only(right: 20, left: 20, top: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(amountTitle),
              Text(
                amount,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).primaryColor),
              ),
            ],
          ),
        ),

        // button appear depend on status
        Padding(
          padding: EdgeInsets.only(left: 8, top: 10),
          child: Row(
            children: [
              // Button Papar
              if (status == "02" || status == "04" || status == "06" || status == "11")
                InkWell(
                  onTap: () {
                    //function for API report
                    trackClick('Paparan Maklumat Status');
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                WTDlistApprovaleClaimInfoScreen(rfdInfoId: rfdInfoId)));
                  },
                  child: ButtonComponent(
                    textBtn: AppLocalizations.of(context).translate('details'),
                    colorBtn: Colors.white,
                    textColor: Theme.of(context).primaryColor,
                  ),
                ),

                if (status == "03")
                InkWell(
                  onTap: () async {
                    print("Borang Kuiri");
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) =>
                    //             WTDlistApprovaleClaimInfoScreen()));

                    showDialog(
                      context: context,
                      builder: (context) {
                        return LoadingIndicator();
                      },
                    );
                    
                    var url = await OpenPdfUtil().loadPdfFromNetwork('${Config.URL}/queryStatusFile');
                    final _extension = path.extension(url.path);

                    if (_extension == ".pdf") {
                      Navigator.of(context, rootNavigator: true).pop();

                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => OpenPDF(file: url),
                        ),
                      );
                    } else {
                      print("Not a PDF file");
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text("The file is not a PDF")),
                      );
                    }
                  },
                  child: ButtonComponent(
                    textBtn: AppLocalizations.of(context).translate('details'),
                    colorBtn: Colors.white,
                    textColor: Theme.of(context).primaryColor,
                  ),
                ),

              // Button Alasan Kuiri
              if (status == "05" || status == "07")
                InkWell(
                  onTap: () {
                    showQueryMsgPopup(context, rfdInfoId);
                  },
                  child: ButtonComponent(
                    textBtn: AppLocalizations.of(context).translate('queryMsg'),
                    colorBtn: Color(0xFF005278),
                  ),
                ),

              // Button DL Borang UMA
              if (status != "01")
                InkWell(
                  onTap: () async {
                    print("to borang page");

                    showDialog(
                      context: context,
                      builder: (context) {
                        return LoadingIndicator();
                      },
                    );
                    
                    var url = await OpenPdfUtil().loadPdfFromNetwork('${Config.URL}/wtdRefund/download/${rfdInfoId}');
                    final _extension = path.extension(url.path);

                    if (_extension == ".pdf") {
                      Navigator.of(context, rootNavigator: true).pop();

                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => OpenPDF(file: url),
                        ),
                      );
                    } else {
                      print("Not a PDF file");
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text("The file is not a PDF")),
                      );
                    }
                  },
                  child: ButtonComponent(
                    textBtn: AppLocalizations.of(context).translate('umaDoc'),
                    colorBtn: Theme.of(context).primaryColor,
                  ),
                ),

              // noButton for status DRAFT
              if (status == "01")
                SizedBox.shrink()
            ],
          ),
        )
      ],
    );
  }

  Color getStatusColor(String status) {
    switch (status) {
      case "01":
        return Color(0xffee8b60);
      case "02":
        return Color(0xfff9cf58);
      case "03":
        return Color(0xffff5963);
      case "04":
        return Color(0xff39d2c0);
      case "05":
        return Color(0xffff5963);
      case "06":
        return Color(0xff39d2c0);
      case "07":
        return Color(0xffff5963);
      case "11":
        return Color(0xff56a855);
      default:
        return Colors.grey;
    }
  }

  Color getTextColor(String status) {
    switch (status) {
      case "01":
        return Color(0xffffffff);
      case "02":
        return Color(0xff333333);
      case "03":
        return Color(0xffffffff);
      case "04":
        return Color(0xffffffff);
      case "05":
        return Color(0xffffffff);
      case "06":
        return Color(0xffffffff);
      case "07":
        return Color(0xffffffff);
      case "11":
        return Color(0xffffffff);
      default:
        return Colors.grey;
    }
  }
}

// status_util.dart
class StatusUtil {
  static String getStatusText(BuildContext context, String status) {
    switch (status) {
      case "01":
        // return 'Deraf';
        return AppLocalizations.of(context).translate('draf');
      case "02":
        // return 'Permohonan Baharu';
        return AppLocalizations.of(context).translate('newApp');
      case "03":
        // return 'Kuiri';
        return AppLocalizations.of(context).translate('query');
      case "04":
        // return 'Pengesahan Dokumen 1';
        return '${AppLocalizations.of(context).translate('verifyDoc')} 1';
      case "05":
        // return 'Kuiri Dokumen 1';
        return '${AppLocalizations.of(context).translate('queryDoc')} 1';
      case "06":
        // return 'Pengesahan Dokumen 2';
        return '${AppLocalizations.of(context).translate('verifyDoc')} 2';
      case "07":
        // return 'Kuiri Dokumen 2';
        return '${AppLocalizations.of(context).translate('queryDoc')} 2';
      case "11":
        // return 'Permohonan Selesai';
        return AppLocalizations.of(context).translate('completeApp');
      default:
        return '-';
    }
  }

  static String getStatusAction(BuildContext context, String status) {
    switch (status) {
      case "01":
        // return 'Permohonan belum dihantar oleh pengguna eGUMIS.';
        return AppLocalizations.of(context).translate('statusAction01');
      case "02":
        // return 'Permohonan telah disahkan dan dihantar untuk proses bayaran.';
        return AppLocalizations.of(context).translate('statusAction02');
      case "03":
        // return 'Permohonan telah ditolak. Sila buat permohonan baharu';
        return AppLocalizations.of(context).translate('statusAction03');
      case "04":
        // return 'Permohonan telah dihantar dan dalam proses pengesahan dokumen 1.';
        return AppLocalizations.of(context).translate('statusAction04');
      case "05":
        // return 'Permohonan dikuiri. Sila rujuk catatan kuiri untuk tindakan.';
        return AppLocalizations.of(context).translate('statusAction05');
      case "06":
        // return 'Permohonan telah disahkan dan dalam proses pengesahan kedua.';
        return AppLocalizations.of(context).translate('statusAction06');
      case "07":
        // return 'Permohonan dikuiri. Sila rujuk catatan kuiri untuk tindakan.';
        return AppLocalizations.of(context).translate('statusAction05');
      case "11":
        // return 'Permohonan telah dibuat pembayaran.';
        return AppLocalizations.of(context).translate('statusAction11');
      default:
        return '-';
    }
  }
}

class ButtonComponent extends StatelessWidget {
  final String textBtn;
  final Color colorBtn;
  final Color textColor;

  const ButtonComponent({
    required this.textBtn,
    required this.colorBtn,
    this.textColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
        side: BorderSide(
          color: Theme.of(context).primaryColor,
          width: 1,
        ),
      ),
      color: colorBtn,
      child: Container(
        height: 40,
        width: 130,
        child: Padding(
          padding: EdgeInsets.only(left: 15, right: 15),
          child: Center(
            child: Text(
              textBtn,
              style: TextStyle(color: textColor, fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}

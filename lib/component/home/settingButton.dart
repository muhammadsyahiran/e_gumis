import 'package:flutter/material.dart';

import '../../screen/settingScreen.dart';

class Settingbutton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SettingScreen()));
      },
      child: Icon(Icons.account_circle_outlined, color: Theme.of(context).primaryColor, size: 25),
    );
  }
}

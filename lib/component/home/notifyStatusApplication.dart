import 'dart:convert';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../../screen/wtdListApprovalScreen.dart';
import '../config.dart';
import '../others/mainTitleCustom.dart';

class NotifyStatusApplication extends StatefulWidget {
  @override
  State<NotifyStatusApplication> createState() => _NotifyStatusApplicationState();
}

class _NotifyStatusApplicationState extends State<NotifyStatusApplication> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MainTitleCustom(title: 'Status Permohonan'),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SizedBox(width: 20),
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => WTDlistAppScreen(filterText: "Permohonan Baharu")));
                },
                child: StatusApplicationComponent(
                  textTitle: 'Permohonan\nBaharu',
                  colorStatus: Color(0xfff9cf58),
                  iconStatus: Icons.feed_outlined,
                  badgeNo: '2',
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => WTDlistAppScreen(filterText: "Permohonan Selesai")));
                },
                child: StatusApplicationComponent(
                  textTitle: 'Permohonan\nSelesai',
                  colorStatus: Color(0xFF96E596),
                  iconStatus: Icons.check_box_outlined,
                  badgeNo: '1',
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => WTDlistAppScreen(filterText: "Pengesahan Dokumen")));
                },
                child: StatusApplicationComponent(
                  textTitle: 'Pengesahan\nDokumen',
                  colorStatus: Color(0xff39d2c0),
                  iconStatus: Icons.content_copy_rounded,
                  badgeNo: '2',
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => WTDlistAppScreen(filterText: "Deraf")));
                },
                child: StatusApplicationComponent(
                  textTitle: 'Deraf',
                  colorStatus: Color(0xfff79b71),
                  iconStatus: Icons.settings_outlined,
                  badgeNo: '1',
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => WTDlistAppScreen(filterText: "Kuiri Dokumen")));
                },
                child: StatusApplicationComponent(
                  textTitle: 'Kuiri\nDokumen',
                  colorStatus: Color(0xfffc6d75),
                  iconStatus: Icons.question_mark_rounded,
                  badgeNo: '1',
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => WTDlistAppScreen(filterText: "Kuiri Permohonan")));
                },
                child: StatusApplicationComponent(
                  textTitle: 'Kuiri\nPermohonan',
                  colorStatus: Color(0xffc33cd9),
                  iconStatus: Icons.flag_outlined,
                  badgeNo: '2',
                ),
              ),
              SizedBox(width: 20),
            ],
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }
}

class StatusApplicationComponent extends StatelessWidget {
  final String textTitle;
  final Color colorStatus;
  final IconData iconStatus;
  final String badgeNo;

  const StatusApplicationComponent({
    required this.textTitle,
    required this.colorStatus,
    required this.iconStatus,
    required this.badgeNo,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: colorStatus,
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
            Badge(
              label: Text(badgeNo),
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xA4FFFFFF),
                  shape: BoxShape.circle,
                ),
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Icon(
                    iconStatus,
                    color: Color(0xff57636c),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Text(
              textTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NotifyStatus extends StatefulWidget {
  @override
  State<NotifyStatus> createState() => _NotifyStatusState();
}

class _NotifyStatusState extends State<NotifyStatus> {
  Map<String, int> statusCounts = {};

  @override
  void initState() {
    super.initState();
    _loadStatusCounts();
  }

  fetchCount() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      final String? tokenValue = prefs.getString('tokenValue');
      final String? identityNumberValue = prefs.getString('identityNumberValue');

      final response = await http.get(
        Uri.parse('${Config.URL}/wtdStatus/${identityNumberValue}'),
        headers: {
          'Authorization': 'Bearer ${tokenValue}',
          'Content-Type': 'application/json',
        },
      );

      if (response.statusCode == 200) {
        final Map<String, dynamic> responseData = json.decode(response.body);
        final List<dynamic> beneficialOwners = responseData['beneficialOwners'];

        // Count occurrences of statusCode
        final Map<String, int> statusCounts = {};
        for (var owner in beneficialOwners) {
          final String statusCode = owner['statusCode'];
          statusCounts[statusCode] = (statusCounts[statusCode] ?? 0) + 1;
        }

        return statusCounts;
      } else {
        print('Error: ${response.statusCode}');
        return {};
      }
    } catch (e) {
      print('Exception: $e');
      return {};
    }
  }

  int getTotalCount(Map<String, int> counts, List<String> codes) {
    int total = 0;
    for (String code in codes) {
      total += counts[code] ?? 0;
    }
    return total;
  }

  String? getBadgeCount(Map<String, int> counts, List<String> codes) {
    int total = getTotalCount(counts, codes);
    return total > 0 ? total.toString() : ""; // Return null if the total is 0
  }

  Future<void> _loadStatusCounts() async {
    final counts = await fetchCount();
    setState(() {
      statusCounts = counts;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MainTitleCustom(title: context.translate('appStatus')),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SizedBox(width: 20),
              StatusAppComponent(
                textTitle: context.translate('draf'),
                colorStatus: Color(0xfff79b71),
                badgeNo: statusCounts['01']?.toString() ?? '',
                navigateTo: WTDlistAppScreen(
                  filterText: context.translate('draf'),
                ),
              ),
              StatusAppComponent(
                textTitle: context.translate('verifyDoc'),
                colorStatus: Color(0xff39d2c0),
                badgeNo: getBadgeCount(statusCounts, [
                  '04',
                  '06'
                ]),
                navigateTo: WTDlistAppScreen(
                  filterText: context.translate('verifyDoc'),
                ),
              ),
              StatusAppComponent(
                textTitle: context.translate('queryDoc'),
                colorStatus: Color(0xfffc6d75),
                badgeNo: getBadgeCount(statusCounts, [
                  '05',
                  '07'
                ]),
                navigateTo: WTDlistAppScreen(
                  filterText: context.translate('queryDoc'),
                ),
              ),
              StatusAppComponent(
                textTitle: context.translate('newApp'),
                colorStatus: Color(0xfff9cf58),
                badgeNo: statusCounts['02']?.toString() ?? '',
                navigateTo: WTDlistAppScreen(
                  filterText: context.translate('newApp'),
                ),
              ),
              StatusAppComponent(
                textTitle: context.translate('completeApp'),
                colorStatus: Color(0xFF96E596),
                badgeNo: statusCounts['11']?.toString() ?? '',
                navigateTo: WTDlistAppScreen(
                  filterText: context.translate('completeApp'),
                ),
              ),
              StatusAppComponent(
                textTitle: context.translate('queryApp'),
                colorStatus: Color(0xffc33cd9),
                badgeNo: statusCounts['03']?.toString() ?? '',
                navigateTo: WTDlistAppScreen(
                  filterText: context.translate('query'),
                ),
              ),
              SizedBox(width: 20),
            ],
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }
}

class StatusAppComponent extends StatefulWidget {
  final String textTitle;
  final Color colorStatus;
  final String? badgeNo;
  final Widget navigateTo;

  const StatusAppComponent({
    required this.textTitle,
    required this.colorStatus,
    this.badgeNo,
    required this.navigateTo,
  });

  @override
  _StatusAppComponentState createState() => _StatusAppComponentState();
}

class _StatusAppComponentState extends State<StatusAppComponent> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => widget.navigateTo),
        );
      },
      child: Padding(
        padding: EdgeInsets.only(top: 8),
        child: widget.badgeNo != ""
            ? Badge(
                label: Text(widget.badgeNo!),
                child: _statusHotspot(),
              )
            : _statusHotspot(),
      ),
    );
  }

  Widget _statusHotspot() {
    return Card(
      color: widget.colorStatus,
      child: Container(
        width: 100,
        height: 50,
        child: Center(
          child: Text(
            widget.textTitle,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}

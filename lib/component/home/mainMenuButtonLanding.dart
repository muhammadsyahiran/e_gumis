import 'package:flutter/material.dart';
import 'package:e_gumis/I10n/localizations.dart';
import '../../screen/faqScreen.dart';
import '../../screen/registerScreen.dart';
import '../../widget/clickTrackingState.dart';
import '../others/contactUs.dart';
import '../others/mainTitleCustom.dart';
import 'package:url_launcher/url_launcher_string.dart';
// import '../others/reportFeedbackPopup.dart';
import 'package:e_gumis/screen/wtdSearchPublicScreen.dart';

class MainMenuLanding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MainTitleCustom(title: AppLocalizations.of(context).translate('mainMenu')),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomIconCard(
              iconData: Icons.question_answer_outlined,
              text: AppLocalizations.of(context).translate('faq'),
              onTap: () {
                //function for API report
                trackClick('Paparan Soalan Lazim');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FaqScreen()),
                );
              },
            ),
            CustomIconCard(
              iconData: Icons.rate_review_outlined,
              text: AppLocalizations.of(context).translate('reportFeedback'),
              onTap: () {
                // showDialog(
                //   context: context,
                //   builder: (BuildContext context) {
                //     return ReportFeedbackPopup();
                //   },
                // );
                //function for API report
                trackClick('Paparan Maklum Balas');
                launchUrlString("https://bit.ly/aduan-egumisjanm");
              },
            ),
            CustomIconCard(
              iconData: Icons.person_add_outlined,
              text: AppLocalizations.of(context).translate('register'),
              onTap: () {
                trackClick('Paparan Pendaftaran');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RegisterScreen()),
                );
              },
            ),
            // CustomIconCard(
            //   iconData: Icons.local_phone_outlined,
            //   text: AppLocalizations.of(context).translate('contactUs'),
            //   onTap: () {
            //     showDialog(
            //       context: context,
            //       builder: (BuildContext context) {
            //         return ContactUs();
            //       },
            //     );
            //   },
            // ),
          ],
        ),
        SizedBox(height: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomIconCard(
              iconData: Icons.search,
              text: AppLocalizations.of(context).translate('searchUM'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => WTDsearchPublicScreen()),
                );
              },
            ),
            CustomIconCard(
              iconData: Icons.local_phone_outlined,
              text: AppLocalizations.of(context).translate('contactUs'),
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return ContactUs();
                  },
                );
              },
            ),
          ],
        ),
        SizedBox(height: 30),
        Padding(
          padding: EdgeInsets.only(left: 40, right: 40),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Text(AppLocalizations.of(context).translate('homeNote'), style: TextStyle(fontSize: 12, overflow: TextOverflow.clip)),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 30),
      ],
    );
  }
}

class CustomIconCard extends StatelessWidget {
  final IconData iconData;
  final String text;
  final VoidCallback onTap;

  const CustomIconCard({
    required this.iconData,
    required this.text,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Card(
            color: Color(0xFFF9CF58),
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Container(
              width: 63,
              height: 63,
              child: Center(
                child: Icon(iconData, color: Color(0xFF003478)),
              ),
            ),
          ),
          Container(
            width: 77,
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}

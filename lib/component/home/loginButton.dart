import 'package:e_gumis/I10n/localizations.dart';
import 'package:flutter/material.dart';

import '../../screen/loginScreen.dart';
import '../../widget/clickTrackingState.dart';

class Loginbutton extends StatelessWidget {
  const Loginbutton({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        trackClick('Paparan Log Masuk');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => LoginScreen(loggedIn: true)));
      },
      child: Column(
        children: [
          Icon(
            Icons.login,
            color: Theme.of(context).primaryColor,
            size: 22,
          ),
          Text(
            context.translate('login'),
            style: TextStyle(
                fontSize: 14,
                color: Colors.black,
                fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '../../I10n/localizations.dart';
import '../../screen/faqScreen.dart';
import '../../screen/wtdListApprovalScreen.dart';
import '../../screen/wtdSearchScreen.dart';
import '../../widget/clickTrackingState.dart';
import '../others/mainTitleCustom.dart';
import 'package:url_launcher/url_launcher_string.dart';
// import '../others/reportFeedbackPopup.dart';

class MainMenuSigneIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MainTitleCustom(title: AppLocalizations.of(context).translate('mainMenu')),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MainMenuButtonComponent(
              iconButton: Icons.search,
              textButton: AppLocalizations.of(context).translate('searchUM'),
              onTap: () {
                //function for API report
                trackClick('Paparan Carian WTD');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => WTDsearchScreen()),
                );
              },
            ),
            MainMenuButtonComponent(
              iconButton: Icons.checklist_rounded,
              textButton: AppLocalizations.of(context).translate('appListUM'),
              onTap: () {
                //function for API report
                trackClick('Paparan Senarai Status WTD');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => WTDlistAppScreen()),
                );
              },
            ),
            MainMenuButtonComponent(
              iconButton: Icons.question_answer_outlined,
              textButton: AppLocalizations.of(context).translate('faq'),
              onTap: () {
                //function for API report
                trackClick('Paparan Soalan Lazim');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FaqScreen()),
                );
              },
            ),
            MainMenuButtonComponent(
              iconButton: Icons.rate_review_outlined,
              textButton: AppLocalizations.of(context).translate('reportFeedback'),
              onTap: () {
                // showDialog(
                //   context: context,
                //   builder: (BuildContext context) {
                //     return ReportFeedbackPopup();
                //   },
                // );
                //function for API report
                trackClick('Paparan Maklum Balas');
                launchUrlString("https://bit.ly/aduan-egumisjanm");
              },
            ),
          ],
        ),
        SizedBox(height: 30),
      ],
    );
  }
}

class MainMenuButtonComponent extends StatelessWidget {
  final String textButton;
  final IconData iconButton;
  final VoidCallback onTap;

  const MainMenuButtonComponent({
    required this.textButton,
    required this.iconButton,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Card(
            color: Color(0xFFABEAFA),
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Container(
              width: 63,
              height: 63,
              child: Icon(
                iconButton,
                color: Theme.of(context).primaryColor,
                size: 24,
              ),
            ),
          ),
          Container(
            width: 77,
            child: Text(
              textButton,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12, color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'strings_en.dart';
import 'strings_ms.dart';

class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);

  static Map<String, Map<String, String>> _localizedValues = {
    'en': englishStrings,
    'ms': malayStrings,
  };

  String translate(String key) {
    // Check if the key exists for the current language code
    if (_localizedValues.containsKey(locale.languageCode)) {
      // Get the map of translations for the current language code
      final translations = _localizedValues[locale.languageCode];
      // Check if the translation exists for the given key
      if (translations != null && translations.containsKey(key)) {
        // Return the translation
        return translations[key]!;
      }
    }
    // Return a default message or the key itself if no translation is found
    return key;
  }

  static AppLocalizations of(BuildContext context) {
    final localizations =
        Localizations.of<AppLocalizations>(context, AppLocalizations);
    assert(localizations != null, 'AppLocalizations must be provided');
    return localizations!;
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ms'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) async {
    return SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}

extension LocalizationExtension on BuildContext {
  String translate(String key) {
    return AppLocalizations.of(this).translate(key);
  }
}
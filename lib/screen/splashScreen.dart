import 'dart:async';
import 'package:e_gumis/widget/vers.dart';
import 'package:flutter/material.dart';
import 'package:e_gumis/screen/homeScreen.dart';
// import 'dart:convert';
// import 'package:e_gumis/screen/profileScreen.dart';
// import 'package:e_gumis/screen/resetPasswordScreen.dart';
// import 'package:http/http.dart' as http;
// import 'package:e_gumis/component/config.dart';
// import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      // loginCheck();
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => HomeScreen()),
        (Route<dynamic> route) => false,
      );
    });
  }

  // Future<void> loginCheck() async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   final String? tokenValue = prefs.getString('tokenValue');
  //   final String? usernameValue = prefs.getString('usernameValue');
  //   // final String? fullnameValue = prefs.getString('fullnameValue');

  //   if (usernameValue != null) {
  //     try {
  //       final response = await http.get(
  //         Uri.parse('${Config.URL}/profile/${usernameValue}'),
  //         headers: {
  //           'Authorization': 'Bearer ${tokenValue}',
  //           'Content-Type': 'application/json',
  //         },
  //       );

  //       if (response.statusCode == 200) {
  //         Map<String, dynamic> responseApi = jsonDecode(response.body);
  //         final user = responseApi['userProfile'];
  //         if (user['firstLogin'] == true || user['passwordExpired'] == true) {
  //           Navigator.pushAndRemoveUntil(
  //             context,
  //             MaterialPageRoute(
  //               builder: (context) => ResetPwdScreen(
  //                 toForceProfile: true,
  //                 isPasswordExpired: user['passwordExpired'])),
  //             (Route<dynamic> route) => false,
  //           );
  //         } else if (user['profileUpdated'] == false) {
  //         Navigator.pushAndRemoveUntil(
  //           context,
  //           MaterialPageRoute(builder: (context) => ProfileScreen(toForceProfile: true)),
  //           (Route<dynamic> route) => false,
  //         );
  //       } else {
  //           setState(() {
  //             Navigator.of(context, rootNavigator: true).pop();
  //           });
  //           Navigator.pushAndRemoveUntil(
  //             context,
  //             MaterialPageRoute(
  //                 builder: (context) => HomeScreen(
  //                     // username: data['username'],
  //                     fullname: user['fullName'])),
  //             (Route<dynamic> route) => false,
  //           );
  //         }
  //         // // have token / valid token
  //         // Navigator.pushReplacement(
  //         //   context,
  //         //   MaterialPageRoute(
  //         //       builder: (context) => HomeScreen(fullname: fullnameValue!)),
  //         // );
  //       } else {
  //         // no token / session token end
  //         navigatHome();
  //       }
  //     } catch (error) {
  //       print(error);
  //       navigatHome();
  //     }
  //   } else {
  //     navigatHome();
  //   }
  // }

  // Future<void> navigatHome() async {
  //   Navigator.pushReplacement(
  //     context,
  //     MaterialPageRoute(builder: (context) => HomeScreen()),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: [
        Expanded(
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: Image.asset('assets/images/loginBG.jpg').image,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset(
                  'assets/images/eGumisLogo.png',
                  width: 230,
                  height: 230,
                  fit: BoxFit.cover,
                ),
                VersionNumber(),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}

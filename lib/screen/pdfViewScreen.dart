import 'dart:io';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:share_plus/share_plus.dart';
import '../widget/timeTrackingState.dart';

class OpenPDF extends StatefulWidget {
  final File file;

  const OpenPDF({required this.file});

  @override
  State<OpenPDF> createState() => _OpenPDFState();
}

class _OpenPDFState extends TimeTrackingState<OpenPDF> {
  @override
  void initState() {
    super.initState();
    //function for API report
    passingInfoTimeTracking('Paparan Borang UMA');
  }

  var path = "No Data";
  Directory? externalDir;

  Future<void> sharePDF() async {
    try {
      if (File(widget.file.path).existsSync()) {
        // Create an XFile from the PDF file path
        final xFile = XFile(widget.file.path);
        // Share the file using shareXFiles
        await Share.shareXFiles(
          [xFile],
        );
      } else {
        print('File does not exist');
      }
    } catch (e) {
      print('Error sharing file: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate("showPdf")),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromARGB(255, 233, 210, 148),
        onPressed: () async {
          await sharePDF();
        },
        child: Icon(Icons.share),
      ),
      body: Column(
        children: [
          Expanded(
            child: PDFView(
              filePath: widget.file.path,
            ),
          ),
        ],
      ),
    );
  }
}

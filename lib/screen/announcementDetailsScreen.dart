import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:e_gumis/I10n/localizations.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_html/flutter_html.dart';
import 'package:photo_view/photo_view.dart';
import '../component/config.dart';
import '../component/others/loadingIndicator.dart';
import '../widget/timeTrackingState.dart';

class AnnouncementDetailsScreen extends StatefulWidget {
  final int paramId;

  AnnouncementDetailsScreen({required this.paramId});

  @override
  _AnnouncementDetailsScreenState createState() => _AnnouncementDetailsScreenState();
}

class _AnnouncementDetailsScreenState extends TimeTrackingState<AnnouncementDetailsScreen> {
  late Future<Map<String, dynamic>> _futureData;

  @override
  void initState() {
    super.initState();
    _futureData = fetchData();

    //function for API report
    passingInfoTimeTracking('Paparan Pengumuman');
  }

  Future<Map<String, dynamic>> fetchData() async {
    final response = await http.get(Uri.parse('${Config.URL}/announcement/${widget.paramId}'));
    if (response.statusCode == 200) {
      return json.decode(response.body)['announcement'];
    } else {
      throw Exception('Failed to load data');
    }
  }

  Widget buildImageContainer(Uint8List decodedImageBytes, BuildContext context) {
    return FutureBuilder<ui.Image>(
      future: decodeImage(decodedImageBytes),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: LoadingIndicator());
        } else if (snapshot.hasError || !snapshot.hasData) {
          return Center(child: Text('Failed to load image'));
        } else {
          final ui.Image image = snapshot.data!;
          final double aspectRatio = image.width / image.height;

          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FullScreenImageViewer(
                    imageProvider: MemoryImage(decodedImageBytes),
                  ),
                ),
              );
            },
            child: Container(
              width: double.infinity,
              child: AspectRatio(
                aspectRatio: aspectRatio,
                child: PhotoView(
                  imageProvider: MemoryImage(decodedImageBytes),
                  minScale: PhotoViewComputedScale.contained,
                  maxScale: PhotoViewComputedScale.covered,
                  backgroundDecoration: BoxDecoration(color: Colors.white),
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget parseHtmlContent(String htmlContent, BuildContext context) {
    final imageRegex = RegExp(r'<img[^>]+src="([^">]+)"');
    final match = imageRegex.firstMatch(htmlContent);

    if (match != null) {
      final imageUrl = match.group(1);
      if (imageUrl != null) {
        if (imageUrl.startsWith('data:image/')) {
          final base64String = imageUrl.split(',')[1];
          final decodedImage = base64Decode(base64String);
          return Column(
            children: [
              buildImageContainer(decodedImage, context),
              SizedBox(height: 15),
              Html(
                data: htmlContent.replaceAll(RegExp(r'<img[^>]*>'), ''),
                style: {
                  "body": Style(
                    textAlign: TextAlign.justify,
                  ),
                },
              ),
            ],
          );
        } else {
          return Column(
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => FullScreenImageViewer(
                        imageProvider: NetworkImage(imageUrl),
                      ),
                    ),
                  );
                },
                child: Container(
                  width: double.infinity,
                  height: 300,
                  child: PhotoView(
                    imageProvider: NetworkImage(imageUrl),
                    minScale: PhotoViewComputedScale.contained,
                    maxScale: PhotoViewComputedScale.covered,
                    backgroundDecoration: BoxDecoration(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Html(
                data: htmlContent.replaceAll(RegExp(r'<img[^>]*>'), ''),
                style: {
                  "body": Style(
                    textAlign: TextAlign.justify,
                  ),
                },
              ),
            ],
          );
        }
      }
    }

    return Html(
      data: htmlContent.replaceAll(RegExp(r'style="[^"]*"'), ''),
    );
  }

  Future<ui.Image> decodeImage(Uint8List imageData) async {
    final codec = await ui.instantiateImageCodec(imageData);
    final frame = await codec.getNextFrame();
    return frame.image;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<Map<String, dynamic>>(
        future: _futureData,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData) {
            return Center(child: Text('No data found'));
          } else {
            final data = snapshot.data!;
            return Scaffold(
              appBar: AppBar(
                title: Text(context.translate('announcement')),
              ),
              body: SingleChildScrollView(
                child: Stack(
                  children: [
                    Container(
                      height: 300,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/janmBuilding.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(25, 200, 25, 25),
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(25.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                data[context.translate('titleUrl')],
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(height: 15),
                              Text(
                                'JANM - ${data['publishOn'].substring(0, 10)}',
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                              SizedBox(height: 15),
                              parseHtmlContent(data[context.translate('bodyUrl')],context),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }
}

class FullScreenImageViewer extends StatelessWidget {
  final ImageProvider imageProvider;

  FullScreenImageViewer({required this.imageProvider});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Center(
        child: PhotoView(
          imageProvider: imageProvider,
          minScale: PhotoViewComputedScale.contained,
          maxScale: PhotoViewComputedScale.covered * 4,
          backgroundDecoration: BoxDecoration(color: Colors.black),
        ),
      ),
    );
  }
}

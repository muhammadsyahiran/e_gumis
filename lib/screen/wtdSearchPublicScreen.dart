import 'dart:convert';
import 'package:flutter/material.dart';
import '../I10n/localizations.dart';
import '../component/config.dart';
import 'package:http/http.dart' as http;
import '../component/others/loadingIndicator.dart';
import '../service/apiReport.dart';

class WTDsearchPublicScreen extends StatefulWidget {
  @override
  State<WTDsearchPublicScreen> createState() => _WTDsearchPublicScreenState();
}

class _WTDsearchPublicScreenState extends State<WTDsearchPublicScreen> {
  TextEditingController searchController = TextEditingController();
  int? _results;
  String _errorSearch = '';
  bool _showNote = true;

  void _showLoading() {
    showDialog(
      context: context,
      builder: (context) {
        return LoadingIndicator();
      },
    );
  }

  void _closeLoading() {
    Navigator.of(context, rootNavigator: true).pop();
  }

  void _wtdSearch(BuildContext context) async {
    var language = AppLocalizations.of(context);

    String searchField = searchController.text;
    // String searchField = '700124025128'; //eija
    // String searchField = '691021106281'; //jat
    // String searchField = '950728106748'; //wife

    setState(() {
      _errorSearch = '';
      _results = null;
      _showNote = true; 
    });

    if (searchField.isEmpty) {
      setState(() {
        _errorSearch = language.translate('errorSearch');
      });
      return;
    }

      // Check for minimum and maximum length
    if (searchField.length < 5 || searchField.length > 12) {
      setState(() {
        _errorSearch = language.translate('errorSearchLength');
      });
      return;
    }

    setState(() {
      _showLoading();
    });

    try {
      final response = await http.get(Uri.parse('${Config.URL}/wtdPublicSearch/${searchController.text}'));

      print(response.body);

      if (response.statusCode == 200) {
        _closeLoading();
        final responseApi = json.decode(response.body);
        final int result = responseApi['wtdListCount'];

        setState(() {
          _showNote = false;
          _results = result;
        });

        //function for API report
        ApiService().reportWtdTotalSearchPublic(searchController.text);
      } else {
        _closeLoading();
        setState(() {
          _showNote = false;
        });
        print('Failed to search');
      }
    } catch (e) {
      _closeLoading();
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('WTD')),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.white,
              Color(0xAA3a6198)
            ],
            stops: [
              0.5,
              1
            ],
            begin: AlignmentDirectional(1, -0.5),
            end: AlignmentDirectional(-1, 0.5),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocalizations.of(context).translate('instructionMsg'),
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                    child: Card(
                      child: TextFormField(
                        // autofocus: true,
                        controller: searchController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: AppLocalizations.of(context).translate('search'),
                          labelStyle: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Theme.of(context).primaryColor,
                              width: 2,
                            ),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          contentPadding: EdgeInsetsDirectional.fromSTEB(16, 12, 16, 12),
                        ),
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                        ),
                        cursorColor: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  SizedBox(width: 5),
                  InkWell(
                    onTap: () {
                      _wtdSearch(context);
                    },
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: Icon(
                          Icons.search,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    _errorSearch,
                    style: TextStyle(color: Colors.red),
                  ),
                ],
              ),
              SizedBox(height: 14),
              _results == null
                  ? (_showNote
                      // Display landing WTD search screen
                      ? Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Card(
                                color: Color(0xFFFFF9DE),
                                elevation: 0,
                                child: Padding(
                                  padding: EdgeInsets.all(16),
                                  child: Text(
                                    context.translate('wtdNotePublic'),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      // Display no result
                      : Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Column(
                          children: [
                            Text('${context.translate('totalNoWtd')} : 0 WTD', style: TextStyle(fontSize: 21)),
                            SizedBox(height: 40),
                            Card(
                              color: Color(0xFFFFF9DE),
                              elevation: 0,
                              child: Padding(
                                padding: EdgeInsets.all(16),
                                child: Text(
                                  context.translate('wtdNotePublic'),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ))
                  // Display number result
                  : Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Column(
                          children: [
                            Text('${context.translate('totalNoWtd')} : ${_results} WTD', style: TextStyle(fontSize: 21)),
                            SizedBox(height: 40),
                            Card(
                              color: Color(0xFFFFF9DE),
                              elevation: 0,
                              child: Padding(
                                padding: EdgeInsets.all(16),
                                child: Text(
                                  context.translate('wtdNotePublic'),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}

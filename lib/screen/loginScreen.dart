import 'package:e_gumis/I10n/localizations.dart';
import 'package:e_gumis/controller/loginController.dart';
import 'package:flutter/material.dart';
import '../component/login/errorText.dart';
import '../component/login/inputDecoration.dart';
import '../component/others/pageTransition.dart';
import '../widget/timeTrackingState.dart';
import '../widget/vers.dart';
import 'forgotPwdScreen.dart';
import 'homeScreen.dart';

class LoginScreen extends StatefulWidget {
  final bool loggedIn;

  LoginScreen({required this.loggedIn});
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends TimeTrackingState<LoginScreen> {
  final loginController = LoginController();

  @override
  void initState() {
    super.initState();
    //function for API report
    passingInfoTimeTracking('Paparan Log Masuk');
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: widget.loggedIn
              ? IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: Icon(Icons.arrow_back),
                )
              : IconButton(
                  onPressed: () => Navigator.pushAndRemoveUntil(
                    context,
                    CustomPageRoute(child: HomeScreen()),
                    (Route<dynamic> route) => false,
                  ),
                  icon: Icon(Icons.arrow_back),
                ),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/loginBG.jpg'),
                fit: BoxFit.cover),
          ),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Center(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                width: 190,
                                child: Image.asset('assets/images/eGumisLogo.png')),
                            SizedBox(height: 30),
                            TextField(
                              controller: loginController.usernameController,
                              decoration:
                                  DecorationComponent.inputDecoration(context).copyWith(
                                labelText: context.translate('userName'),
                              ),
                            ),
                            ErrorText(message: loginController.errorUserName),
                            SizedBox(height: 20.0),
                            TextField(
                              controller: loginController.passwordController,
                              decoration:
                                  DecorationComponent.inputDecoration(context).copyWith(
                                labelText: context.translate('password'),
                                suffixIcon: Padding(
                                  padding: const EdgeInsets.only(right: 16),
                                  child: IconButton(
                                      icon: Icon(
                                        loginController.obscureText
                                            ? Icons.visibility_off
                                            : Icons.visibility,
                                      ),
                                      onPressed: () {
                                        loginController.togglePasswordVisibility(setState);
                                      }),
                                ),
                              ),
                              obscureText: loginController.obscureText,
                            ),
                            ErrorText(message: loginController.errorPassword),
                            ErrorText(message: loginController.errorMessage),
                            SizedBox(height: 20.0),
                            Card(
                              elevation: 4,
                              color: Color(0xAA003478),
                              child: InkWell(
                                onTap: () {
                                  loginController.loginAPI(context, setState);
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  width: 230,
                                  height: 52,
                                  child: Text(
                                    context.translate('login'),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold, color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 20),
                            TextButton(
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ForgotPasswordScreen()),
                                  );
                                },
                                child: Text(
                                  context.translate('forgotPassword'),
                                  style: TextStyle(
                                      color: Colors.black, fontWeight: FontWeight.bold),
                                )),
                            SizedBox(height: 20),
                            if (loginController.passwordIncorrect)
                              Card(
                                color: Color(0xFFFFF9DE),
                                child: Padding(
                                  padding: EdgeInsets.all(15),
                                  child: Text(
                                    '${context.translate('loginErrorBefore')}  ${loginController.remainingAttempts}  ${context.translate('loginErrorAfter')}',
                                  ),
                                ),
                              ),
                            if (loginController.accountLock)
                              Card(
                                elevation: 0,
                                color: Color(0xFFf2dede),
                                child: Padding(
                                  padding: EdgeInsets.all(15),
                                  child: Text(
                                    context.translate('loginErrorLocked'),
                                    style: TextStyle(color: Color(0xffa94442)),
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              VersionNumber(),
              SizedBox(height: 40)
            ],
          ),
        ),
      ),
    );
  }
}

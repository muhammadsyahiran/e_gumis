import 'dart:convert';
import 'package:e_gumis/screen/announcementDetailsScreen.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import '../I10n/localizations.dart';
import '../component/config.dart';
import '../component/others/loadingIndicator.dart';
import '../component/wtdListApproval/cardComponent.dart';
import '../widget/clickTrackingState.dart';
import '../widget/timeTrackingState.dart';

class AnnouncementListScreen extends StatefulWidget {
  @override
  State<AnnouncementListScreen> createState() => _AnnouncementListScreenState();
}

class _AnnouncementListScreenState extends TimeTrackingState<AnnouncementListScreen> {
  List<dynamic> data = [];
  List<dynamic> filteredData = [];
  TextEditingController searchController = TextEditingController();
  bool showClearButton = false;
  ScrollController _scrollController = ScrollController();
  bool _loading = true;

  @override
  void initState() {
    super.initState();
    fetchAnnouncement();
    searchController.addListener(() {
      filterData();
    });
    //function for API report
    passingInfoTimeTracking('Paparan Senarai Pengumuman');
  }

  Future<void> fetchAnnouncement() async {
    try {
      final response = await http.get(Uri.parse('${Config.URL}/announcement'));
      if (response.statusCode == 200) {
        setState(() {
          Map<String, dynamic> jsonResponse = jsonDecode(response.body);
          List<dynamic> announcements = jsonResponse['announcements'];

          data = announcements.map((announcement) {
            if (announcement is Map<String, dynamic>) {
              return announcement;
            } else {
              print('Unexpected announcement format: $announcement');
              return {};
            }
          }).toList();
          _loading = false;
          filteredData = List.from(data);
          filterData();
        });
      } else {
        throw Exception('Failed to load data');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  void filterData() {
    var language = AppLocalizations.of(context);

    setState(() {
      filteredData = data.where((item) {
        final title = item[language.translate('titleUrl')].toString().toLowerCase();
        final date = item['publishOn'].toString().toLowerCase();
        final announcement = item[language.translate('bodyUrl')].toString().toLowerCase();
        final searchText = searchController.text.toLowerCase();

        showClearButton = searchText.isNotEmpty;

        return title.contains(searchText) || date.contains(searchText) || announcement.contains(searchText);
      }).toList();
    });
  }

  void clearSearch() {
    setState(() {
      searchController.clear();
      showClearButton = false;
      filteredData = List.from(data); // Reset filtered data
    });
  }

  // void dispose() {
  //   _scrollController.dispose(); // Step 3: Dispose the ScrollController
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(context.translate('announcementList')),
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 20, right: 20),
        child: _loading
            ? LoadingIndicator()
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(
                        child: Card(
                          child: TextFormField(
                            controller: searchController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText:
                                  '${context.translate('search')} ${context.translate('announcement')}',
                              labelStyle: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Theme.of(context).primaryColor,
                                  width: 2,
                                ),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              contentPadding: EdgeInsetsDirectional.fromSTEB(
                                  16, 12, 16, 12),
                            ),
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                            ),
                            cursorColor: Theme.of(context).primaryColor,
                          ),
                        ),
                      ),
                      if (showClearButton)
                        Padding(
                          padding: const EdgeInsets.only(left: 3),
                          child: Card(
                            child: InkWell(
                              onTap: clearSearch,
                              child: Padding(
                                padding: const EdgeInsets.all(12),
                                child: Icon(
                                  Icons.clear,
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                  SizedBox(height: 7),
                  Row(
                    children: [
                      SizedBox(width: 5),
                      Text(
                          '${context.translate('totalList')} : ${filteredData.length}'),
                    ],
                  ),
                  SizedBox(height: 7),
                  if (filteredData.length != 0)
                    Expanded(
                      child: ListView.builder(
                        controller: _scrollController,
                        itemCount: filteredData.length + 1,
                        itemBuilder: (context, index) {
                          if (index == filteredData.length) {
                            // Footer item
                            return Padding(
                              padding: const EdgeInsets.all(20),
                              child: Center(
                                child: Text(context.translate('endList')),
                              ),
                            );
                          }

                          return Card(
                            child: Padding(
                              padding: EdgeInsets.all(15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // Image carousel
                                  // SizedBox(
                                  //   height: 200, // Adjust height as needed
                                  //   child: PageView(
                                  //     children: [
                                  //       Image.asset(
                                  //           'assets/images/janmBuilding.jpg'),
                                  //       Image.asset(
                                  //           'assets/images/janmBuilding.jpg'),
                                  //       Image.asset(
                                  //           'assets/images/janmBuilding.jpg'),
                                  //     ],
                                  //   ),
                                  // ),
                                  // SizedBox(height: 10),
                                  Text(
                                    filteredData[index][context.translate('titleUrl')],
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(height: 10),
                                  Text(filteredData[index][context.translate('bodyUrl')].length > 100
                                    ? '${filteredData[index][context.translate('bodyUrl')].substring(0, 100)}...'
                                    : filteredData[index][context.translate('bodyUrl')]),
                                  SizedBox(height: 10),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.calendar_month,
                                        color: Theme.of(context).primaryColor,
                                        size: 13,
                                      ),
                                      SizedBox(width: 4),
                                      Text(
                                        'JANM - ${filteredData[index]['publishOn'].substring(0, 10)}',
                                        style: TextStyle(
                                            fontSize: 13,
                                            color:
                                                Theme.of(context).primaryColor),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          //function for API report
                                          trackClick('Paparan Info Pengumuman');
                                          print(filteredData[index]['id']);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      AnnouncementDetailsScreen(
                                                          paramId: filteredData[
                                                              index]['id'])));
                                        },
                                        child: ButtonComponent(
                                          colorBtn:
                                              Theme.of(context).primaryColor,
                                          textBtn: context
                                              .translate('announcementBtn'),
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  if (filteredData.length == 0)
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.search_off_rounded,
                            size: 80,
                            color: Theme.of(context).primaryColor,
                          ),
                        ],
                      ),
                    ),
                ],
              ),
      ),
    );
  }
//SizedBox(height: 10),
  // // Image carousel below the search bar
  // Container(
  //   height: 180, // Set the height for the image carousel
  //   child: ListView.builder(
  //     scrollDirection: Axis.horizontal,
  //     itemCount: imagePaths.length,
  //     itemBuilder: (context, index) {
  //       return Padding(
  //         padding: const EdgeInsets.only(right: 8),
  //         child: ClipRRect(
  //           borderRadius: BorderRadius.circular(10),
  //           child: Image.asset(
  //             imagePaths[index],
  //             width: 345, // Set width for each image
  //             fit: BoxFit.cover,
  //           ),
  //         ),
  //       );
  //     },
  //   ),
  // ),
}

import 'package:e_gumis/I10n/localizations.dart';
import 'package:e_gumis/screen/pdfViewScreen.dart';
import 'package:e_gumis/widget/vers.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';
import '../component/config.dart';
import '../component/others/loadingIndicator.dart';
import '../component/others/logout.dart';
import '../component/others/contactUs.dart';
import '../controller/pdfController.dart';
import '../widget/clickTrackingState.dart';
import '../widget/switchLanguage.dart';
import '../widget/timeTrackingState.dart';
import 'resetPasswordScreen.dart';
import 'profileScreen.dart';
import 'package:path/path.dart' as path;

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends TimeTrackingState<SettingScreen> {
   @override
  void initState() {
    super.initState();
    //function for API report
    passingInfoTimeTracking('Paparan Tetapan');
  }

  @override
  Widget build(BuildContext context) {
    // var languageProvider = Provider.of<LanguageProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(context.translate('setting')),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.white, Color(0x6A3A6198)],
            stops: [0.4, 1],
            begin: AlignmentDirectional(0, -1),
            end: AlignmentDirectional(0, 1),
          ),
        ),
        child: Padding(
          padding: EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          SizedBox(height: 40),
                          CardComponent(
                            text: context.translate('profile'),
                            icon: Icons.person_sharp,
                            onTap: () {
                              //function for API report
                              trackClick('Paparan Profil');
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ProfileScreen(),
                                ),
                              );
                            },
                          ),
                          CardComponent(
                            text: context.translate('resetPwd'),
                            icon: Icons.restart_alt_outlined,
                            onTap: () {
                              //function for API report
                              trackClick('Paparan Set Semula Kata Laluan');
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ResetPwdScreen(),
                                ),
                              );
                            },
                          ),
                          CardComponent(
                            text: context.translate('language'),
                            icon: Icons.language,
                            onTap: () {},
                            switchWidget: SwitchWidget(),
                          ),
                          CardComponent(
                            text: context.translate('privacyPolicy'),
                            icon: Icons.privacy_tip_outlined,
                            onTap: () async {
                              //function for API report
                              trackClick('Paparan Dasar Privasi Aplikasi');  
                              await launchUrlString('https://10.23.32.127/privacy?lang=${context.translate('langURL')}');
                            },
                          ),
                          CardComponent(
                            text: context.translate('contactUs'),
                            icon: Icons.contact_phone_outlined,
                            onTap: () {
                              //function for API report
                              trackClick('Paparan Maklumat BWTD');  
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return ContactUs();
                                },
                              );
                            },
                          ),
                          CardComponent(
                            text: context.translate('userManual'),
                            icon: Icons.menu_book_outlined,
                            onTap: () async {
                              //function for API report
                              trackClick('Paparan Manual Pengguna');  

                              // launchUrlString('https://tourism.gov.in/sites/default/files/2019-04/dummy-pdf_2.pdf');
                              // launchUrlString('https://docs.google.com/gview?embedded=true&url=https://tourism.gov.in/sites/default/files/2019-04/dummy-pdf_2.pdf');
                              //   print('to user manual link >>>>> "https://egumis.anm.gov.my/useraManual"');

                              showDialog(
                                context: context,
                                builder: (context) {
                                  return LoadingIndicator();
                                },
                              );
                              
                              var url = await OpenPdfUtil().loadPdfFromNetwork('${Config.URL}/userManual');
                              final _extension = path.extension(url.path);

                              if (_extension == ".pdf") {
                                Navigator.of(context, rootNavigator: true).pop();

                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => OpenPDF(file: url),
                                  ),
                                );
                              } else {
                                print("Not a PDF file");
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text("The file is not a PDF")),
                                );
                              }
                            },
                          ),
                          CardComponent(
                            text: context.translate('logOut'),
                            icon: Icons.logout_outlined,
                            onTap: () async {
                              showLogOutPopup(context,
                                  title: context.translate('logOut'));
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              VersionNumber(),
              SizedBox(height: 40)
            ],
          ),
        ),
      ),
    );
  }
}

class LanguageProvider extends ChangeNotifier {
  Locale _locale = Locale('ms', ''); // Default locale is Malay

  Locale get locale => _locale;

  void setLocale(Locale newLocale) {
    _locale = newLocale;
    notifyListeners();
  }
}

class CardComponent extends StatelessWidget {
  final String text;
  final IconData icon;
  final VoidCallback onTap;
  final Widget? switchWidget;

  const CardComponent({
    required this.text,
    required this.icon,
    required this.onTap,
    this.switchWidget,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Card(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Icon(icon),
                      SizedBox(width: 20),
                      Text(text),
                    ],
                  ),
                  if (switchWidget != null) switchWidget!,
                  if (switchWidget == null)
                    Icon(Icons.keyboard_arrow_right_rounded)
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}

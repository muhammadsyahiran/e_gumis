import 'dart:convert';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:e_gumis/component/config.dart';
import 'package:e_gumis/screen/profileScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../component/login/errorText.dart';
import '../component/login/inputDecoration.dart';
import '../component/others/loadingIndicator.dart';
import '../component/others/logout.dart';
import '../component/others/successfullPopup.dart';
import 'package:http/http.dart' as http;
import '../service/apiReport.dart';
import '../widget/timeTrackingState.dart';

class ResetPwdScreen extends StatefulWidget {
  final bool toForceProfile;
  final bool isPasswordExpired;

  ResetPwdScreen({this.toForceProfile = false, this.isPasswordExpired = false});
  @override
  ResetPwdScreenState createState() => ResetPwdScreenState();
}

class ResetPwdScreenState extends TimeTrackingState<ResetPwdScreen> {
  final TextEditingController _currentPasswordController = TextEditingController();
  final TextEditingController _newPasswordController = TextEditingController();
  final TextEditingController _repeatPasswordController = TextEditingController();

  bool _obscureTextTemp = true;
  bool _obscureTextNewPwd = true;
  bool _obscureTextRepeat = true;
  String _errortempPassword = '';
  String _errornewPassword = '';
  String _errorrepeatPassword = '';
  String _errorPasswordCriteria = '';
  bool _loading = false;

  // Password criteria flags
  bool _hasMinLength = false;
  bool _hasUppercase = false;
  bool _hasLowercase = false;
  bool _hasNumber = false;
  bool _hasSpecialChar = false;

  @override
  void initState() {
    super.initState();
    //function for API report
    passingInfoTimeTracking('Paparan Set Semula Kata Laluan');
  }

  void _togglePasswordVisibilityTemp() {
    setState(() {
      _obscureTextTemp = !_obscureTextTemp;
    });
  }
  void _togglePasswordVisibilityNewPwd() {
    setState(() {
      _obscureTextNewPwd = !_obscureTextNewPwd;
    });
  }
  void _togglePasswordVisibilityRepeat() {
    setState(() {
      _obscureTextRepeat = !_obscureTextRepeat;
    });
  }

  void _validatePassword(String password) {
    setState(() {
      _hasMinLength = password.length >= 12;
      _hasUppercase = password.contains(RegExp(r'[A-Z]'));
      _hasLowercase = password.contains(RegExp(r'[a-z]'));
      _hasNumber = password.contains(RegExp(r'\d'));
      _hasSpecialChar = password.contains(RegExp(r'[@$!%*?&]'));
    });
  }

  Future<void> _updatePwdCheckField(context) async {
    var language = AppLocalizations.of(context);

    String tempPwd = _currentPasswordController.text;
    String newPwd = _newPasswordController.text;
    String repeatPwd = _repeatPasswordController.text;

    setState(() {
      _errortempPassword = '';
      _errornewPassword = '';
      _errorrepeatPassword = '';
      _errorPasswordCriteria = '';
    });

    if (tempPwd.isEmpty || newPwd.isEmpty || repeatPwd.isEmpty) {
      setState(() {
        if (tempPwd.isEmpty) _errortempPassword = _errortempPassword = widget.toForceProfile == true ? language.translate('errorReqTempPassword') : language.translate('errorReqCurrentPassword');
        if (newPwd.isEmpty) _errornewPassword = language.translate('errorReqNewPassword');
        if (repeatPwd.isEmpty) _errorrepeatPassword = language.translate('errorReqRepeatPassword');
      });
      return;
    }

    if (newPwd.length < 12 || repeatPwd.length < 12) {
      setState(() {
        if (newPwd.length < 12) _errornewPassword = language.translate('errorCharPassword');
        if (repeatPwd.length < 12) _errorrepeatPassword = language.translate('errorCharPassword');
      });
      return;
    }

    // Validate password criteria
    final passwordRegex = RegExp(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{12,}$');
    if (!passwordRegex.hasMatch(newPwd)) {
      setState(() {
        _errorPasswordCriteria = language.translate('errorPasswordCriteria');
      });
      return;
    }

    if (newPwd != repeatPwd) {
      setState(() {
        _errorrepeatPassword = language.translate('errorRepeatPassword');
      });
      return;
    }

    await _apiUpdatePwdCheckField(context);
  }

  Future<void> _apiUpdatePwdCheckField(context) async {
    var language = AppLocalizations.of(context);
    _loading = true;

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? tokenValue = prefs.getString('tokenValue');
    final String? usernameValue = prefs.getString('usernameValue');
    final String? fullNameValue = prefs.getString('fullnameValue');
    final String? identityNumberValue = prefs.getString('identityNumberValue');
    try {
      final response = await http.put(Uri.parse('${Config.URL}/changePassword/${usernameValue}'),
          headers: {
            'Authorization': 'Bearer ${tokenValue}',
            'Content-Type': 'application/json',
          },
          body: jsonEncode({
            "currentPassword": _currentPasswordController.text,
            "newPassword": _newPasswordController.text,
            "confirmPassword": _repeatPasswordController.text
          }));
      final data = jsonDecode(response.body);

      // Check if the response status is OK
      if (data['message'] == "Password successfully updated") {
        setState(() {
          _loading = false;
        });

        void _pushToProfile() {
          if (widget.toForceProfile == true) Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileScreen(toForceProfile: true)));
        }

        showDialog(
          context: context,
          builder: (BuildContext context) {
            // Future.delayed(Duration(seconds: 4), () {
            // Navigator.of(context).pop(true);
            // _pushToProfile();
            // });
            return SuccessfullPopup(
              title: context.translate('successUpdate'),
              onTap: () {
                Navigator.of(context).pop();
                _pushToProfile();
              },
            );
          },
        );

        if (widget.toForceProfile != true) {
          ApiService().reportResetPassword(usernameValue!, fullNameValue!, identityNumberValue!);
        }
      } else {
        _loading = false;
        print('Status code: ${response.statusCode}');
        setState(() {
          _errortempPassword = widget.toForceProfile == true ? language.translate('errorTempPassword') : language.translate('errorCurrentPassword');
        });
      }
    } catch (e) {
      _loading = false;
      print('Error: $e');
      setState(() {
        _errortempPassword = "An error occurred. Please try again.";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(context.translate('updatePassword')),
        actions: [
          if (widget.toForceProfile == true)
            Padding(
              padding: const EdgeInsets.only(right: 16),
              child: InkWell(
                  onTap: () {
                    showLogOutPopup(context, title: context.translate('logOut'));
                  },
                  child: Icon(Icons.logout)),
            )
        ],
      ),
      body: _loading
          ? LoadingIndicator()
          : SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    widget.toForceProfile
                        ? Text(context.translate('instructionUpdatePassword'))
                        : widget.isPasswordExpired
                            ? Text('kata laluan anda telah tamat tempoh. ${context.translate('instructionUpdatePassword')}')
                            : SizedBox.shrink(),
                    SizedBox(height: 16),
                    TextField(
                      controller: _currentPasswordController,
                      decoration: DecorationComponent.inputDecoration(context).copyWith(
                        labelText: widget.toForceProfile == true ? context.translate('tempPassword') : context.translate('currentPassword'),
                        suffixIcon: Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: IconButton(
                            icon: Icon(
                              _obscureTextTemp ? Icons.visibility_off : Icons.visibility,
                            ),
                            onPressed: _togglePasswordVisibilityTemp,
                          ),
                        ),
                      ),
                      obscureText: _obscureTextTemp,
                    ),
                    ErrorText(message: _errortempPassword),
                    SizedBox(height: 16),
                    TextField(
                      controller: _newPasswordController,
                      onChanged: _validatePassword,
                      decoration: DecorationComponent.inputDecoration(context).copyWith(
                        labelText: context.translate('newPassword'),
                        suffixIcon: Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: IconButton(
                            icon: Icon(
                              _obscureTextNewPwd ? Icons.visibility_off : Icons.visibility,
                            ),
                            onPressed: _togglePasswordVisibilityNewPwd,
                          ),
                        ),
                      ),
                      obscureText: _obscureTextNewPwd,
                    ),
                    ErrorText(message: _errornewPassword),
                    SizedBox(height: 16),
                    ErrorText(message: _errorPasswordCriteria),

                    // Password criteria with dynamic checkmarks
                    Text(
                      context.translate('newPwdTypeInstruc'),
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(height: 10),
                    _buildCriteriaRow(context.translate('atLeast12Char'), _hasMinLength),
                    _buildCriteriaRow(context.translate('containUppercase'), _hasUppercase),
                    _buildCriteriaRow(context.translate('containLowercase'), _hasLowercase),
                    _buildCriteriaRow(context.translate('contain1Number'), _hasNumber),
                    _buildCriteriaRow(context.translate('contain1SpecialChar'), _hasSpecialChar),

                    SizedBox(height: 16),
                    TextField(
                      controller: _repeatPasswordController,
                      decoration: DecorationComponent.inputDecoration(context).copyWith(
                        labelText: context.translate('repeatPassword'),
                        suffixIcon: Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: IconButton(
                            icon: Icon(
                              _obscureTextRepeat ? Icons.visibility_off : Icons.visibility,
                            ),
                            onPressed: _togglePasswordVisibilityRepeat,
                          ),
                        ),
                      ),
                      obscureText: _obscureTextRepeat,
                    ),
                    ErrorText(message: _errorrepeatPassword),
                    SizedBox(height: 20.0),
                    Card(
                      elevation: 4,
                      color: Color(0xAA003478),
                      child: InkWell(
                        onTap: () {
                          _updatePwdCheckField(context);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 52,
                          child: Text(
                            context.translate('update'),
                            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Widget _buildCriteriaRow(String text, bool isValid) {
    return Row(
      children: [
        Icon(
          isValid ? Icons.check_circle : Icons.radio_button_unchecked,
          color: isValid ? Colors.green : Colors.grey,
        ),
        SizedBox(width: 8),
        Text(
          text,
          style: TextStyle(
            color: isValid ? Colors.green : Colors.grey,
            fontWeight: isValid ? FontWeight.bold : FontWeight.normal,
          ),
        ),
      ],
    );
  }
}

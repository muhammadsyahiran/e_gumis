import 'dart:convert';
import 'package:accordion/accordion.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../component/login/errorText.dart';
import '../component/config.dart';
import '../component/login/inputDecoration.dart';
import '../component/others/loadingIndicator.dart';
import '../component/others/logout.dart';
import '../component/others/successfullPopup.dart';
import '../widget/timeTrackingState.dart';
import 'homeScreen.dart';

class ProfileScreen extends StatefulWidget {
  final bool toForceProfile;

  ProfileScreen({this.toForceProfile = false});

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends TimeTrackingState<ProfileScreen> {
  final TextEditingController _controllerUserName = TextEditingController();
  final TextEditingController _controllerFullName = TextEditingController();
  final TextEditingController _controllerIcType = TextEditingController();
  final TextEditingController _controllerIcNumber = TextEditingController();
  final TextEditingController _controllerEmail = TextEditingController();
  final TextEditingController _controllerAddress1 = TextEditingController();
  final TextEditingController _controllerAddress2 = TextEditingController();
  final TextEditingController _controllerAddress3 = TextEditingController();
  final TextEditingController _controllerCity = TextEditingController();
  final TextEditingController _controllerPoscode = TextEditingController();
  final TextEditingController _controllerPhoneNo = TextEditingController();
  final TextEditingController _controllerOfficeNo = TextEditingController();

  String _errorFullName = '';
  String _errorAddress1 = '';
  String _errorAddress2 = '';
  String _errorAddress3 = '';
  String _errorState = '';
  String _errorCountry = '';
  String _errorCity = '';
  String _errorPoscode = '';
  String _errorPersonalPhone = '';
  String _errorOfficePhone = '';
  String? _dropdownState;
  String? _dropdownCountry;
  Map<String, String> stateMap = {};
  Map<String, String> countryMap = {};
  bool _loading = true;
  late Map<String, dynamic> user;

  @override
  void initState() {
    super.initState();
    fetchUser();
  }

  Future<void> fetchUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? tokenValue = prefs.getString('tokenValue');
    final String? usernameValue = prefs.getString('usernameValue');

    try {
      final response = await http.get(
        Uri.parse('${Config.URL}/profile/${usernameValue}'),
        headers: {
          'Authorization': 'Bearer ${tokenValue}',
          'Content-Type': 'application/json',
        },
      );

      final responseCountry = await http.get(Uri.parse('${Config.URL}/country'));
      Map<String, dynamic> responseCountryApi = json.decode(responseCountry.body);
      List<dynamic> countriesApi = responseCountryApi['countries'];
      setState(() {
        countryMap = {
          for (var country in countriesApi) country['code']: country['nameEn']
        };
      });
      // print(countriesApi);

      final responseState = await http.get(Uri.parse('${Config.URL}/state'));
      Map<String, dynamic> responseStateApi = json.decode(responseState.body);
      List<dynamic> statesApi = responseStateApi['states'];
      setState(() {
        stateMap = {
          for (var state in statesApi) state['code']: state['nameEn']
        };
      });
      // print(statesApi);

      if (response.statusCode == 200) {
        Map<String, dynamic> responseApi = jsonDecode(response.body);
        Map<String, dynamic> identityTypeMap = {
          'INDV': context.translate('idTypeINDV'),
          'INTL': context.translate('idTypeINTL')
        };

        user = responseApi['userProfile'];

        setState(() {
          _controllerUserName.text = user['username'] ?? '';
          _controllerFullName.text = user['fullName'] ?? '';
          _controllerIcType.text = identityTypeMap[user['identityType']] ?? '';
          _controllerIcNumber.text = user['identityNumber'] ?? '';
          _controllerEmail.text = user['email'] ?? '';
          _controllerAddress1.text = user['address1'] ?? '';
          _controllerAddress2.text = user['address2'] ?? '';
          _controllerAddress3.text = user['address3'] ?? '';
          _controllerCity.text = user['city'] ?? '';
          _controllerPoscode.text = user['postcode'] ?? '';
          _dropdownState = stateMap[user['state']] ?? '';
          _dropdownCountry = countryMap[user['country']] ?? '';
          _controllerPhoneNo.text = user['mobileNo'] ?? '';
          _controllerOfficeNo.text = user['officeNo'] ?? '';
          _loading = false;
        });
      }

      //function for API report
      passingInfoTimeTracking('Paparan Profil');
    } catch (error) {
      print('Error fetching user: $error');
      setState(() {
        _loading = false;
      });
    }
  }

  void _updateProfile(BuildContext context) {
    String fullName = _controllerFullName.text;
    String address1 = _controllerAddress1.text;
    String address2 = _controllerAddress2.text;
    String address3 = _controllerAddress3.text;
    String city = _controllerCity.text;
    String poscode = _controllerPoscode.text;
    String personalPhone = _controllerPhoneNo.text;
    String officeHomePhone = _controllerOfficeNo.text;

    // Clear existing errors
    setState(() {
      _errorFullName = '';
      _errorAddress1 = '';
      _errorAddress2 = '';
      _errorAddress3 = '';
      _errorState = '';
      _errorCountry = '';
      _errorCity = '';
      _errorPoscode = '';
      _errorPersonalPhone = '';
      _errorOfficePhone = '';
    });

    bool hasError = false;

    // Validate Full Name
    if (fullName.isEmpty) {
      _errorFullName = context.translate('fullNameError');
      hasError = true;
    } else if (fullName.length < 3 || fullName.length > 150) {
      _errorFullName = context.translate('fullNameErrorLength');
      hasError = true;
    }

    // Validate Address
    if (address1.isEmpty) {
      _errorAddress1 = context.translate('addressError');
      hasError = true;
    } else if (address1.length > 35) {
      _errorAddress1 = context.translate('addressErrorLength');
      hasError = true;
    }
    if (address2.length > 35) {
      _errorAddress2 = context.translate('addressErrorLength');
      hasError = true;
    }
    if (address3.length > 35) {
      _errorAddress3 = context.translate('addressErrorLength');
      hasError = true;
    }

    // Validate State & Country
    if (_dropdownState == null || _dropdownState!.isEmpty) {
      _errorState = context.translate('stateError');
      hasError = true;
    }
    if (_dropdownCountry == null || _dropdownCountry!.isEmpty) {
      _errorCountry = context.translate('countryError');
      hasError = true;
    }

    // Validate City
    if (city.isEmpty) {
      _errorCity = context.translate('cityError');
      hasError = true;
    } else if (city.length > 40) {
      _errorCity = context.translate('cityErrorlength');
      hasError = true;
    }

    // Validate Poscode
    if (poscode.isEmpty) {
      _errorPoscode = context.translate('postcodeError');
      hasError = true;
    } else if (poscode.length < 5 || poscode.length > 10) {
      _errorPoscode = context.translate('postcodeErrorlength');
      hasError = true;
    }

    // Validate Phone Numbers
    if (personalPhone.isEmpty && officeHomePhone.isEmpty) {
      _errorPersonalPhone = context.translate('numberMobileError');
      _errorOfficePhone = context.translate('numberMobileError');
      hasError = true;
    }
    if (personalPhone.isNotEmpty && (personalPhone.length < 10 || personalPhone.length > 30)) {
      _errorPersonalPhone = context.translate('numberMobileErrorLength');
      hasError = true;
    }
    if (officeHomePhone.isNotEmpty && (officeHomePhone.length < 7 || officeHomePhone.length > 30)) {
      _errorOfficePhone = context.translate('numberOfficeErrorLength');
      hasError = true;
    }

    // If there are errors, stop execution
    if (hasError) {
      setState(() {});
      return;
    }

    // Proceed to update profile
    _updateUserProfile(context);
  }

  Future<void> _updateUserProfile(BuildContext context) async {
    setState(() {
      _loading = true;
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? tokenValue = prefs.getString('tokenValue');
    final String? usernameValue = prefs.getString('usernameValue');

    final response = await http.put(
      Uri.parse('${Config.URL}/profile/${usernameValue}'),
      headers: {
        'Authorization': 'Bearer ${tokenValue}',
        'Content-Type': 'application/json'
      },
      body: jsonEncode({
        'fullName': _controllerFullName.text,
        'address1': _controllerAddress1.text,
        'address2': _controllerAddress2.text,
        'address3': _controllerAddress3.text,
        'city': _controllerCity.text,
        'postcode': _controllerPoscode.text,
        'state': stateMap.entries.firstWhere((entry) => entry.value == _dropdownState).key,
        'country': countryMap.entries.firstWhere((entry) => entry.value == _dropdownCountry).key,
        'mobileNo': _controllerPhoneNo.text,
        'officeNo': _controllerOfficeNo.text,
      }),
    );

    if (response.statusCode == 200) {
      setState(() {
        _loading = false;
      });

      void _pushToHome() {
        if (widget.toForceProfile == true) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeScreen(fullname: user['fullName'])),
            (Route<dynamic> route) => false,
          );
        }
      }

      showDialog(
        context: context,
        builder: (BuildContext context) {
          // Future.delayed(Duration(seconds: 4), () {
          // Navigator.of(context).pop(true);
          // _pushToHome();
          // });
          return SuccessfullPopup(
            title: context.translate('successUpdate'),
            onTap: () {
              Navigator.of(context).pop();
              _pushToHome();
            },
          );
        },
      );

      // fetchUser();
    } else if (response.statusCode == 400) {
      setState(() {
        _loading = false;
      });
      print('update profile Failed');
    } else {
      setState(() {
        _loading = false;
      });
      print('internal server error');
      // setState(() {
      //   _errorMessage = 'Registration failed. Please try again.';
      // });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          context.translate('profile'),
        ),
        actions: [
          if (widget.toForceProfile == true)
            Padding(
              padding: const EdgeInsets.only(right: 16),
              child: InkWell(
                  onTap: () {
                    showLogOutPopup(context, title: context.translate('logOut'));
                  },
                  child: Icon(Icons.logout)),
            )
        ],
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.white,
              Color(0x6A3A6198)
            ],
            stops: [
              0.4,
              1
            ],
            begin: AlignmentDirectional(0, -1),
            end: AlignmentDirectional(0, 1),
          ),
        ),
        child: _loading
            ? LoadingIndicator()
            : SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(15, 0, 15, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Accordion(
                        rightIcon: Icon(
                          Icons.keyboard_arrow_down,
                          color: Colors.black,
                        ),
                        disableScrolling: true,
                        headerBorderWidth: 1,
                        headerBorderColor: Theme.of(context).primaryColor,
                        headerBackgroundColor: Colors.white,
                        contentBorderColor: Theme.of(context).primaryColor,
                        contentBorderWidth: 1,
                        contentHorizontalPadding: 20,
                        contentVerticalPadding: 20,
                        scaleWhenAnimating: true,
                        // openAndCloseAnimation: true,
                        headerPadding: const EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                        children: [
                          AccordionSection(
                            header: CustomText(text: context.translate('accInfo')),
                            content: Column(
                              children: [
                                CustomSubText(text: context.translate('userName')),
                                CustomSubTextField(
                                  controller: _controllerUserName,
                                  isReadOnly: true,
                                ),
                              ],
                            ),
                          ),
                          AccordionSection(
                            header: CustomText(text: context.translate('personalInfo')),
                            content: Column(
                              children: [
                                CustomSubText(text: context.translate('fullName')),
                                CustomSubTextField(controller: _controllerFullName, hintText: context.translate('fullNameHint')),
                                ErrorText(message: _errorFullName),
                                CustomSubText(text: context.translate('idType')),
                                CustomSubTextField(
                                  controller: _controllerIcType,
                                  isReadOnly: true,
                                ),
                                CustomSubText(text: context.translate('idNumber')),
                                CustomSubTextField(
                                  controller: _controllerIcNumber,
                                  isReadOnly: true,
                                ),
                                CustomSubText(text: context.translate('email')),
                                CustomSubTextField(
                                  controller: _controllerEmail,
                                  isReadOnly: true,
                                ),
                              ],
                            ),
                          ),
                          AccordionSection(
                            isOpen: true,
                            header: CustomText(text: context.translate('contactInfo')),
                            content: Column(
                              children: [
                                CustomSubText(text: '${context.translate('address')} 1'),
                                CustomSubTextField(controller: _controllerAddress1, hintText: '${context.translate('address')} 1'),
                                ErrorText(message: _errorAddress1),
                                CustomSubText(text: '${context.translate('address')} 2'),
                                CustomSubTextField(controller: _controllerAddress2, hintText: '${context.translate('address')} 2'),
                                ErrorText(message: _errorAddress2),
                                CustomSubText(text: '${context.translate('address')} 3'),
                                CustomSubTextField(controller: _controllerAddress3, hintText: '${context.translate('address')} 3'),
                                ErrorText(message: _errorAddress3),
                                CustomSubText(text: context.translate('city')),
                                CustomSubTextField(controller: _controllerCity, hintText: context.translate('city')),
                                ErrorText(message: _errorCity),
                                CustomSubText(text: context.translate('postcode')),
                                CustomSubTextField(controller: _controllerPoscode, hintText: context.translate('postcode'), keyboardType: TextInputType.number),
                                ErrorText(message: _errorPoscode),
                                CustomSubText(text: context.translate('state')),
                                SizedBox(height: 7),
                                DropdownSearch<String>(
                                  items: stateMap.values.toList(),
                                  selectedItem: _dropdownState,
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      _dropdownState = newValue;
                                    });
                                  },
                                  popupProps: PopupProps.menu(
                                    showSearchBox: true,
                                    searchFieldProps: TextFieldProps(
                                      decoration: InputDecoration(
                                        fillColor: Colors.white,
                                        hintText: context.translate('stateSearch'),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                                      ),
                                    ),
                                  ),
                                  dropdownDecoratorProps: DropDownDecoratorProps(
                                    dropdownSearchDecoration: DecorationComponent.inputDecoration(context, hintText: context.translate('stateSelect')),
                                  ),
                                ),
                                ErrorText(message: _errorState),
                                CustomSubText(text: context.translate('country')),
                                SizedBox(height: 7),
                                DropdownSearch<String>(
                                  items: countryMap.values.toList(),
                                  selectedItem: _dropdownCountry,
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      _dropdownCountry = newValue;
                                    });
                                  },
                                  popupProps: PopupProps.menu(
                                    showSearchBox: true,
                                    searchFieldProps: TextFieldProps(
                                      decoration: InputDecoration(
                                        fillColor: Colors.white,
                                        hintText: context.translate('countrySearch'),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                                      ),
                                    ),
                                  ),
                                  dropdownDecoratorProps: DropDownDecoratorProps(
                                    dropdownSearchDecoration: DecorationComponent.inputDecoration(context, hintText: context.translate('countrySearch')),
                                  ),
                                ),
                                ErrorText(message: _errorCountry),
                                SizedBox(height: 30),
                                Text(context.translate('instructionForMobileInsert')),
                                SizedBox(height: 15),
                                CustomSubText(text: context.translate('numberMobile')),
                                CustomSubTextField(controller: _controllerPhoneNo, hintText: context.translate('numberMobile'), keyboardType: TextInputType.number),
                                ErrorText(message: _errorPersonalPhone),
                                CustomSubText(text: context.translate('numberOffice')),
                                CustomSubTextField(controller: _controllerOfficeNo, hintText: context.translate('numberOffice'), keyboardType: TextInputType.number),
                                ErrorText(message: _errorOfficePhone),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 30),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly, // Aligns cards with space in between
                        children: [
                          Card(
                            elevation: 4,
                            color: Color(0xAA003478),
                            child: InkWell(
                              onTap: () {
                                _updateProfile(context);
                              },
                              child: Container(
                                alignment: Alignment.center,
                                height: 45,
                                width: 138, // You can set a specific width for the card
                                child: Text(
                                  context.translate('update'),
                                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          if (widget.toForceProfile == false)
                            Card(
                              elevation: 4,
                              color: Color(0xAA003478),
                              child: InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  height: 45,
                                  width: 138, // You can set a specific width for the card
                                  child: Text(
                                    context.translate('cancel'),
                                    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                      SizedBox(height: 100),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}

class CustomSubTextField extends StatelessWidget {
  final TextEditingController controller;
  final String? hintText;
  final bool isReadOnly;
  final TextInputType? keyboardType;

  const CustomSubTextField({required this.controller, this.hintText, this.isReadOnly = false, this.keyboardType});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: controller,
          cursorColor: Color(0xAA000080),
          decoration: InputDecoration(
            hintText: hintText,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xAA000080)),
            ),
            fillColor: isReadOnly ? Colors.grey.shade200 : Colors.transparent,
            filled: true,
          ),
          readOnly: isReadOnly,
          keyboardType: keyboardType,
        ),
        SizedBox(height: 15),
      ],
    );
  }
}

class CustomSubText extends StatelessWidget {
  final String text;

  const CustomSubText({required this.text});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          text,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ],
    );
  }
}

class CustomText extends StatelessWidget {
  final String text;

  const CustomText({required this.text});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              text,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ],
        ),
      ],
    );
  }
}

import 'dart:convert';
import 'package:e_gumis/component/others/loadingIndicator.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../I10n/localizations.dart';
import '../component/config.dart';
import '../component/wtdListApproval/cardComponent.dart';
import '../widget/timeTrackingState.dart';

class WTDlistAppScreen extends StatefulWidget {
  final String filterText;
  WTDlistAppScreen({this.filterText = ""});

  @override
  State<WTDlistAppScreen> createState() => _WTDlistAppScreenState();
}

class _WTDlistAppScreenState extends TimeTrackingState<WTDlistAppScreen> {
  TextEditingController searchController = TextEditingController();
  bool showClearButton = false;
  bool loading = true;
  List<dynamic> data = [];
  List<dynamic> filteredData = [];
  // String _totalAmount = '';

  @override
  void initState() {
    super.initState();
    searchController = TextEditingController(text: widget.filterText);
    fetchData();
    searchController.addListener(() {
      filterData();
    });

    //function for API report
    passingInfoTimeTracking('Paparan Senarai Status WTD');
  }

  Future<void> fetchData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? tokenValue = prefs.getString('tokenValue');
    final String? identityNumberValue = prefs.getString('identityNumberValue');

    final response = await http.get(Uri.parse('${Config.URL}/wtdStatus/${identityNumberValue}'),
      headers: {
        'Authorization': 'Bearer ${tokenValue}',
        'Content-Type': 'application/json',
      },
    );

    Map<String, dynamic> responseApi = jsonDecode(response.body);
    final wtdStatus = responseApi['beneficialOwners'] as List<dynamic>;

    if (response.statusCode == 200) {
      setState(() {
        if (wtdStatus.isEmpty || (wtdStatus.length == 1 && wtdStatus.first.isEmpty)) {
          filteredData = [];
        } else {
          data = wtdStatus;
          filteredData = List.from(data);
        }
          loading = false;
          filterData();
          // final total = wtdStatus.fold(0.0, (sum, item) {
          //   final claimAmount = item['claimAmount'];
          //   return sum + (claimAmount != null
          //           ? double.parse(claimAmount.toString())
          //           : 0.0);
          // });
          // _totalAmount = total.toStringAsFixed(2);
        });
      } else {
        setState(() {
          loading = false;
        });
      throw Exception('Failed to load data');
    }
  }

  void filterData() {
    setState(() {
      final searchText = searchController.text.toLowerCase();
      final filterCriteria = widget.filterText.toLowerCase();

      showClearButton = searchText.isNotEmpty;

      filteredData = data.where((item) {
        final refNo = item['referenceNumber'].toString().toLowerCase();
        final date = item['date'].toString().toLowerCase();
        final amount = item['claimAmount'].toString().toLowerCase();
        final status = StatusUtil.getStatusText(context, item['statusCode']).toLowerCase();
        final action = StatusUtil.getStatusAction(context, item['statusCode']) .toLowerCase();

        return refNo.contains(searchText) ||
            date.contains(searchText) ||
            amount.contains(searchText) ||
            status.contains(searchText) ||
            action.contains(searchText) &&
            status.contains(filterCriteria);
      }).toList();
    });
  }

  void clearSearch() {
    setState(() {
      searchController.clear();
      showClearButton = false;
      filteredData = List.from(data);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text(context.translate('umApp')) ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.white, Color(0xAA3a6198)],
            stops: [0.5, 1],
            begin: AlignmentDirectional(1, -0.5),
            end: AlignmentDirectional(-1, 0.5),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Row(
                children: [
                  Expanded(
                    child: Card(
                      child: TextFormField(
                        controller: searchController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: context.translate('searchMultiple'),
                          labelStyle: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Theme.of(context).primaryColor,
                              width: 2,
                            ),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          contentPadding:
                              EdgeInsetsDirectional.fromSTEB(16, 12, 16, 12),
                        ),
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                        ),
                        cursorColor: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  if (showClearButton)
                    Padding(
                      padding: const EdgeInsets.only(left: 3),
                      child: Card(
                        child: InkWell(
                          onTap: clearSearch,
                          child: Padding(
                            padding: const EdgeInsets.all(12),
                            child: Icon(
                              Icons.clear,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                ],
              ),
              SizedBox(height: 7),
              Padding(
                padding: const EdgeInsets.only(left: 5, right: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        '${context.translate('totalList')} : ${filteredData.length}'),
                    // Text('Jumlah WTD : ${_totalAmount}'),
                    Text(''),
                  ],
                ),
              ),
              SizedBox(height: 7),
              loading
                  ? Expanded(child: Center(child: LoadingIndicator()))
                  : filteredData.isNotEmpty
                      ? Expanded(
                          child: ListView.builder(
                            itemCount: filteredData.length + 1,
                            itemBuilder: (context, index) {
                              if (index == filteredData.length) {
                                return Padding(
                                  padding: const EdgeInsets.all(20),
                                  child: Center(
                                    child: Text(context.translate('endList')),
                                  ),
                                );
                              }
                              return Column(
                                children: [
                                  Card(
                                    child: Padding(
                                      padding: EdgeInsets.only(top: 20, bottom: 20),
                                      child: CardComponent(
                                        dateTitle: context.translate('date'),
                                        dateSubTitle: filteredData[index]['date'] ?? '',
                                        // .substring(0, 10),
                                        refNoTitle: context.translate('refNumber'),
                                        refNoSubTitle: filteredData[index]['referenceNumber'] ?? '',
                                        actionTitle: context.translate('action'),
                                        status: filteredData[index]['statusCode'] ?? '',
                                        amountTitle: context.translate('amount'),
                                        amount: 'RM ${filteredData[index]['claimAmount'] ?? ''}',
                                        rfdInfoId: filteredData[index]['rfdInfoId'] ?? '',
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        )
                      : Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.search_off_rounded,
                                    size: 80,
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  SizedBox(height: 10),
                                  Text(context.translate('noRecord'),
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
            ],
          ),
        ),
      ),
    );
  }
}

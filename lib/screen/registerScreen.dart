import 'dart:convert';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:e_gumis/screen/loginScreen.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';
import '../component/config.dart';
import '../component/login/errorText.dart';
import '../component/login/inputDecoration.dart';
import '../component/others/loadingIndicator.dart';
import '../component/others/successfullPopup.dart';
import 'package:http/http.dart' as http;
import '../service/apiReport.dart';
import '../widget/timeTrackingState.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends TimeTrackingState<RegisterScreen> {
  @override
  void initState() {
    super.initState();
    //function for API report
    passingInfoTimeTracking('Paparan Pendaftaran');
  }

  Map<String, String> dropdownItems = {};
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    dropdownItems = {
      'INDV': AppLocalizations.of(context).translate('idTypeINDV'),
      'INTL': AppLocalizations.of(context).translate('idTypeINTL')
    };
  }

  final TextEditingController _userFullNameController = TextEditingController();
  final TextEditingController _userIDnPassportNoController = TextEditingController();
  final TextEditingController _userEmailController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();
  final RegExp emailRegex = RegExp(r'^[^@]+@[^@]+\.[^@]+');

  bool _agreedToTerm = false;

  String _errorFullName = '';
  String? _dropdownValue = 'INDV';
  String _errorNoIC = '';
  String _errorEmail = '';
  String _errorUserName = '';
  String _errorDropdown = '';
  String _errorMessage = '';
  String _errorUserNameExistMessage = '';

  Future<void> _formRegister(BuildContext context) async {
    String userFullName = _userFullNameController.text;
    String userIDnPassportNo = _userIDnPassportNoController.text;
    String userEmail = _userEmailController.text;
    String userName = _userNameController.text;
    bool hasError = false;

    setState(() {
      _errorFullName = '';
      _errorDropdown = '';
      _errorNoIC = '';
      _errorEmail = '';
      _errorUserName = '';
      _errorMessage = '';
      _errorUserNameExistMessage = '';
    });

    if (userFullName.isEmpty) {
      _errorFullName = context.translate('fullNameError');
      hasError = true;
    }
    if (_dropdownValue == null) {
      _errorDropdown = context.translate('errorIdType');
      hasError = true;
    }
    if (userIDnPassportNo.isEmpty) {
      _errorNoIC = context.translate('errorID');
      hasError = true;
    } else if (userIDnPassportNo.length > 20) {
      _errorNoIC = context.translate('errorIDlength');
      hasError = true;
    }
    if (userEmail.isEmpty) {
      _errorEmail = context.translate('errorEmail');
      hasError = true;
    } else if (!emailRegex.hasMatch(userEmail)) {
      _errorEmail = context.translate('errorEmailFormat');
      hasError = true;
    }
    if (userName.isEmpty) {
      _errorUserName = context.translate('userNameError');
      hasError = true;
    }
    if (!_agreedToTerm) {
      _errorMessage = context.translate('errorTermCond');
      hasError = true;
    }
    if (hasError) {
      setState(() {});
      return;
    }

    try {
      showDialog(
        context: context,
        builder: (context) {
          return LoadingIndicator();
        },
      );

      final response1 = await http.post(
        Uri.parse('${Config.URL}/register'),
        headers: {
          'Content-Type': 'application/json'
        },
        body: jsonEncode(
          {
            "fullName": _userFullNameController.text,
            "identityType": _dropdownValue,
            "identityNumber": _userIDnPassportNoController.text,
            "email": _userEmailController.text,
            "username": _userNameController.text,
            "language": context.translate('selectLang'),
          },
        ),
      );

      if (response1.statusCode == 200) {
        // Successfully registered
        Navigator.of(context, rootNavigator: true).pop();

        // void _clearFields() {
        //   setState(() {
        //     _userFullNameController.clear();
        //     _dropdownValue = null;
        //     _userIDnPassportNoController.clear();
        //     _userEmailController.clear();
        //     _userNameController.clear();
        //   });
        // }

        showDialog(
          context: context,
          builder: (BuildContext context) {
            // Future.delayed(Duration(seconds: 4), () {
            // Navigator.of(context).pop(true);
            // _clearFields();
            // });
            return SuccessfullPopup(
              title: context.translate('successReg'),
              subTitle: context.translate('successRegMsg'),
              onTap: () {
                Navigator.of(context).pop();
                // _clearFields();
                Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => LoginScreen(
                      loggedIn: false,
                    )),
            (Route<dynamic> route) => false);
              },
            );
          },
        );

        //function for API report
        ApiService().reportUserRegistration(_userNameController.text, _userFullNameController.text, _userIDnPassportNoController.text);
      
      } else if (response1.statusCode == 400) {
        final Map<String, dynamic> responseBody = jsonDecode(response1.body);
        final List<dynamic> errors = responseBody['errors'];
        final errorMsg = errors[0]['message'];

        if (errorMsg == "User with the identity number already exists") {
          _errorNoIC = context.translate('errorMsgExistingID');
        }
        if (errorMsg == "User with the email already exists") {
          _errorEmail = context.translate('errorMsgExistingEmail');
        }
        if (errorMsg == "User with the username already exists") {
          _errorUserName = context.translate('errorMsgExistingUsername');
        }

        setState(() {
          Navigator.of(context, rootNavigator: true).pop();
          _errorMessage = context.translate('errorMsgExistingUsernameEmailID');
        });
      } else {
        setState(() {
          Navigator.of(context, rootNavigator: true).pop();
          _errorMessage = context.translate('errorMsgFailedReg');
        });
      }
    } catch (e) {
      setState(() {
        Navigator.of(context, rootNavigator: true).pop();
        _errorMessage = 'An error occurred please contact support';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(context.translate('userReg')),
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.white,
              Color(0xAA3a6198)
            ],
            stops: [
              0.5,
              1
            ],
            begin: AlignmentDirectional(1, -0.5),
            end: AlignmentDirectional(-1, 0.5),
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 30),
            child: Column(
              children: <Widget>[
                TextField(
                  controller: _userFullNameController,
                  decoration: DecorationComponent.inputDecoration(context).copyWith(
                    labelText: context.translate('fullName'),
                    hintText: context.translate('hintFullName'),
                  ),
                ),
                ErrorText(message: _errorFullName),
                SizedBox(height: 10),
                DropdownButtonFormField<String>(
                  hint: Text(context.translate('idType')),
                  value: _dropdownValue,
                  decoration: DecorationComponent.inputDecoration(context),
                  style: TextStyle(color: Theme.of(context).primaryColor),
                  onChanged: (String? newValue) {
                    setState(() {
                      _dropdownValue = newValue!;
                    });
                  },
                  items: dropdownItems.entries.map<DropdownMenuItem<String>>((entry) {
                    return DropdownMenuItem<String>(
                      value: entry.key,
                      child: Text(entry.value),
                    );
                  }).toList(),
                ),
                ErrorText(message: _errorDropdown),
                SizedBox(height: 10),
                TextField(
                  controller: _userIDnPassportNoController,
                  decoration: DecorationComponent.inputDecoration(context).copyWith(labelText: AppLocalizations.of(context).translate('idNumber'), hintText: _dropdownValue == 'INDV' ? '903456789876' : 'A34567892'),
                  keyboardType: _dropdownValue == 'INDV' ? TextInputType.number : TextInputType.text,
                ),
                ErrorText(message: _errorNoIC),
                SizedBox(height: 10),
                TextField(
                  controller: _userEmailController,
                  decoration: DecorationComponent.inputDecoration(context).copyWith(
                    labelText: context.translate('email'),
                    hintText: context.translate('hintEmail'),
                  ),
                ),
                ErrorText(message: _errorEmail),
                SizedBox(height: 10),
                TextField(
                  controller: _userNameController,
                  decoration: DecorationComponent.inputDecoration(context).copyWith(
                    labelText: context.translate('userName'),
                    hintText: '${context.translate('hintUserName')}: syamsul9066',
                  ),
                ),
                ErrorText(message: _errorUserNameExistMessage),
                ErrorText(message: _errorUserName),
                SizedBox(height: 20),
                InkWell(
                  splashColor: Colors.transparent,
                  onTap: () {
                    launchUrlString("https://egumis.anm.gov.my/terms?lang=${context.translate('langURL')}");
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => Termnconditionscreen()));
                  },
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(text: context.translate('textSpan1'), style: TextStyle(color: Colors.black)),
                        TextSpan(
                            text: context.translate('textSpan2'),
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                            )),
                        TextSpan(text: context.translate('textSpan3'), style: TextStyle(color: Colors.black))
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Checkbox(
                      value: _agreedToTerm,
                      onChanged: (bool? newValue) async {
                        setState(() => _agreedToTerm = newValue!);
                      },
                      side: BorderSide(
                        width: 2,
                        color: Theme.of(context).primaryColor,
                      ),
                      activeColor: Theme.of(context).primaryColor,
                      checkColor: Colors.white,
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      child: Text(context.translate('acknowledgement'), textAlign: TextAlign.start),
                    ),
                  ],
                ),
                ErrorText(message: _errorMessage),
                SizedBox(height: 20.0),
                Card(
                  elevation: 4,
                  color: Theme.of(context).primaryColor,
                  child: InkWell(
                    onTap: () {
                      _formRegister(context);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      height: 50,
                      child: Text(
                        context.translate('signUp'),
                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Card(
                  color: Color(0xFFFFF9DE),
                  elevation: 0,
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      children: [
                        Icon(
                          Icons.warning_rounded,
                          color: Color(0xAAf9cf58),
                          size: 80,
                        ),
                        SizedBox(width: 10),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(context.translate('importantInfo'),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  )),
                              SizedBox(height: 5),
                              Text(
                                context.translate('importantInfoBody'),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

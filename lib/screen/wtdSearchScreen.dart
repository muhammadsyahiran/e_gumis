import 'dart:async';
import 'dart:convert';
import 'package:e_gumis/screen/pdfViewScreen.dart';
import 'package:e_gumis/controller/pdfController.dart';
import 'package:e_gumis/service/apiReport.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../I10n/localizations.dart';
import '../component/config.dart';
import 'package:http/http.dart' as http;
import '../component/others/loadingIndicator.dart';
import 'package:path/path.dart' as path;
import '../widget/timeTrackingState.dart';

class WTDsearchScreen extends StatefulWidget {
  @override
  State<WTDsearchScreen> createState() => _WTDsearchScreenState();
}

class _WTDsearchScreenState extends TimeTrackingState<WTDsearchScreen> {
  TextEditingController searchController = TextEditingController();
  TextEditingController filterController = TextEditingController();
  List<Map<String, dynamic>> _filteredResults = [];
  List<Map<String, dynamic>> _results = [];
  String _errorSearch = '';
  // String _totalAmount = '';
  bool _showNote = true;
  int _searchCount = 0;
  final int _dailySearchLimit = 2;

  @override
  void initState() {
    super.initState();
    _loadSearchCount();

    //function for API report
    passingInfoTimeTracking('Paparan Semakan WTD');
  }

  void _showLoading() {
    showDialog(
      context: context,
      builder: (context) {
        return LoadingIndicator();
      },
    );
  }

  void _closeLoading() {
    Navigator.of(context, rootNavigator: true).pop();
  }

  void _loadSearchCount() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int? savedCount = prefs.getInt('searchCount');
    final String? lastSearchDate = prefs.getString('lastSearchDate');
    final String today = DateTime.now().toIso8601String().substring(0, 10);

    setState(() {
      if (lastSearchDate == today) {
        _searchCount = savedCount ?? 0;
      } else {
        _searchCount = 0; // Reset count for a new day
        prefs.setInt('searchCount', 0);
        prefs.setString('lastSearchDate', today);
      }
    });
  }

  void _incrementSearchCount() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _searchCount += 1;
    });
    prefs.setInt('searchCount', _searchCount);
    prefs.setString(
        'lastSearchDate', DateTime.now().toIso8601String().substring(0, 10));
  }

  void _wtdSearch(BuildContext context) async {
    if (_searchCount >= _dailySearchLimit) {
      setState(() {
        _errorSearch = AppLocalizations.of(context).translate('searchLimitReached');
      });
      return;
    }

    _incrementSearchCount();
    var language = AppLocalizations.of(context);
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? tokenValue = prefs.getString('tokenValue');
    // final String? userNameValue = prefs.getString('usernameValue');
    // final String? fullNameValue = prefs.getString('fullnameValue');
    // final String? identityNumberValue = prefs.getString('identityNumberValue');

    String searchField = searchController.text;
    // String searchField = '700124025128'; //eija
    // String searchField = '691021106281'; //jat
    // String searchField = '950728106748'; //wife

    setState(() {
      _errorSearch = '';
      _results = [];
      _filteredResults = [];
    });

    if (searchField.isEmpty) {
      setState(() {
        _errorSearch = language.translate('errorSearch');
      });
      return;
    }

      // Check for minimum and maximum length
    if (searchField.length < 5 || searchField.length > 12) {
      setState(() {
        _errorSearch = language.translate('errorSearchLength');
      });
      return;
    }

    setState(() {
      _showLoading();
    });

    try {
      final response = await http.get(
        Uri.parse('${Config.URL}/wtdSearch/${searchController.text}'),
        headers: {
          'Authorization': 'Bearer ${tokenValue}',
          'Content-Type': 'application/json',
        },
      );

      if (response.statusCode == 200) {
        _closeLoading();
        final responseApi = json.decode(response.body);

        // Check if the result is null or not a list
        if (responseApi['wtdList'] == null || responseApi['wtdList'] is! List) {
          setState(() {
            _errorSearch = AppLocalizations.of(context).translate('noResultsFound');
            _showNote = false;
          });
          return;
        }

        final List<dynamic> result = responseApi['wtdList'];

        setState(() {
          _showNote = false;
          _results = result.cast<Map<String, dynamic>>();
          _filteredResults = _results;
          // final total =
          //     _filteredResults.fold(0.0, (sum, item) => sum + item['amount']);
          // _totalAmount = total.toStringAsFixed(2);
        });

        //function for API report
        ApiService().reportWtdTotalSearch(searchController.text);
      } else {
        _closeLoading();
        setState(() {
          _showNote = false;
        });
        print('Failed to search');
      }
    } catch (e) {
      _closeLoading();
      print(e);
    }
  }

  void _filterResults() {
    String filterText = filterController.text.toLowerCase();
    setState(() {
      if (filterText.isEmpty) {
        _filteredResults = _results;
      } else {
        _filteredResults = _results.where((item) {
          return item['entityName'].toLowerCase().contains(filterText) || item['wtdTypeName'].toLowerCase().contains(filterText) || item['financialYear'].toLowerCase().contains(filterText) || item['refNo'].toLowerCase().contains(filterText) || item['amount'].toString().contains(filterText);
        }).toList();
      }
    });
  }

  Future<void> _downloadWTDsearch() async {
    setState(() {
      _showLoading();
    });

    var url = await OpenPdfUtil().loadPdfFromNetwork('${Config.URL}/wtdSearch/download/${searchController.text}');
    final _extension = path.extension(url.path);

    if (_extension == ".pdf") {
      _closeLoading();
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => OpenPDF(file: url),
        ),
      );
    } else {
      print("Not a PDF file");
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("The file is not a PDF")),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('WTD')),
        actions: [
          if (_results.isNotEmpty)
            Padding(
              padding: EdgeInsets.only(right: 20),
              child: InkWell(
                onTap: () {
                  _downloadWTDsearch();
                },
                child: Card(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: Color.fromARGB(255, 233, 210, 148),
                      width: 2.0,
                    ),
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12),
                    child: Icon(
                      Icons.sim_card_download_outlined,
                      color: Color(0xFFE7AC10),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.white,
              Color(0xAA3a6198)
            ],
            stops: [
              0.5,
              1
            ],
            begin: AlignmentDirectional(1, -0.5),
            end: AlignmentDirectional(-1, 0.5),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocalizations.of(context).translate('instructionMsg'),
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                    child: Card(
                      child: TextFormField(
                        // autofocus: true,
                        controller: searchController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: AppLocalizations.of(context).translate('search'),
                          labelStyle: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Theme.of(context).primaryColor,
                              width: 2,
                            ),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          contentPadding: EdgeInsetsDirectional.fromSTEB(16, 12, 16, 12),
                        ),
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                        ),
                        cursorColor: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  SizedBox(width: 5),
                  InkWell(
                    onTap: () {
                      _wtdSearch(context);
                    },
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(12),
                        child: Icon(
                          Icons.search,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '${AppLocalizations.of(context).translate('remainingSearches')}: ${_dailySearchLimit - _searchCount}',
                    style: TextStyle(color: Colors.black54),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    _errorSearch,
                    style: TextStyle(color: Colors.red),
                  ),
                ],
              ),
              _showNote
                  ? SizedBox()
                  : _results.isEmpty
                      ? SizedBox()
                      : Row(children: [
                          Expanded(
                            child: Card(
                              child: TextFormField(
                                controller: filterController,
                                autofocus: false,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: context.translate('searchMultiple'),
                                  labelStyle: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Theme.of(context).primaryColor,
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  contentPadding: EdgeInsetsDirectional.fromSTEB(16, 12, 16, 12),
                                ),
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                ),
                                cursorColor: Theme.of(context).primaryColor,
                                onChanged: (value) => _filterResults(),
                              ),
                            ),
                          ),
                          SizedBox(width: 5),
                          Card(
                            elevation: 0,
                            child: Padding(
                              padding: const EdgeInsets.all(12),
                              child: Icon(
                                Icons.filter_alt_outlined,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ]),
              SizedBox(height: 7),
              if (_results.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(left: 5, right: 5),
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('${context.translate('totalList')} : ${_filteredResults.length}'),
                      // Text('Jumlah WTD : ${_totalAmount}'),
                    ],
                  ),
                ),
              SizedBox(height: 7),
              _results.isEmpty
                  ? (_showNote
                      // Display landing WTD search screen
                      ? Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Card(
                                color: Color(0xFFFFF9DE),
                                elevation: 0,
                                child: Padding(
                                  padding: EdgeInsets.all(16),
                                  child: Text(
                                    context.translate('wtdNote'),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      // Display no result
                      : Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.search_off_rounded,
                                    size: 80,
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    context.translate('noRecord'),
                                    style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ))
                  // Display card list result
                  : Expanded(
                      child: ListView.builder(
                        itemCount: _filteredResults.length + 1,
                        itemBuilder: (context, index) {
                          if (index == _filteredResults.length) {
                            // Footer item
                            return Padding(
                              padding: const EdgeInsets.all(20),
                              child: Center(
                                child: Text(context.translate('endList')),
                              ),
                            );
                          }
                          final result = _filteredResults[index];
                          return Card(
                            child: Padding(
                              padding: EdgeInsets.all(20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      TextComponent(
                                        title: AppLocalizations.of(context).translate('ownerName'),
                                        subTitle: result['fullName'],
                                      ),
                                      TextComponent(
                                        title: AppLocalizations.of(context).translate('idNumber'),
                                        subTitle: result['idNumber'],
                                      ),
                                      TextComponent(
                                        title: AppLocalizations.of(context).translate('wtdType'),
                                        subTitle: result['wtdTypeName'],
                                      ),
                                      TextComponent(
                                        title: AppLocalizations.of(context).translate('entityName'),
                                        subTitle: result['entityName'],
                                      ),
                                      TextComponent(
                                        title: AppLocalizations.of(context).translate('refNumber'),
                                        subTitle: result['refNo'],
                                      ),
                                      TextComponent(
                                        title: AppLocalizations.of(context).translate('financeYear'),
                                        subTitle: result['financialYear'],
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Text(AppLocalizations.of(context).translate('amount')),
                                      SizedBox(width: 10),
                                      Text(
                                        'RM ${result['amount']}',
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}

class TextComponent extends StatelessWidget {
  final String title;
  final String subTitle;

  const TextComponent({
    required this.title,
    required this.subTitle,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(fontSize: 12),
        ),
        Text(
          subTitle,
          style: TextStyle(
            fontWeight: FontWeight.w600,
            color: Theme.of(context).primaryColor,
          ),
        ),
        SizedBox(height: 10)
      ],
    );
  }
}

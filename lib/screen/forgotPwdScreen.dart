import 'dart:convert';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:e_gumis/screen/loginScreen.dart';
import 'package:flutter/material.dart';
import '../component/config.dart';
import '../component/login/errorText.dart';
import '../component/login/inputDecoration.dart';
import '../component/others/loadingIndicator.dart';
import '../component/others/successfullPopup.dart';
import 'package:http/http.dart' as http;

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final TextEditingController _userEmailController = TextEditingController();
  final RegExp emailRegex = RegExp(r'^[^@]+@[^@]+\.[^@]+');

  String _errorEmail = '';
  String _errorMessage = '';
  bool hasError = false;

  Future<void> _submitForgotPassword(BuildContext context) async {
    String userEmail = _userEmailController.text;

    setState(() {
      _errorEmail = '';
      _errorMessage = '';
      hasError = false;
    });

    if (userEmail.isEmpty) {
      _errorEmail = context.translate('errorEmail');
      hasError = true;
    } else if (!emailRegex.hasMatch(userEmail)) {
      _errorEmail = context.translate('errorEmailFormat');
      hasError = true;
    }
    if (hasError) {
      setState(() {});
      return;
    }

    try {
      showDialog(
        context: context,
        builder: (context) {
          return LoadingIndicator();
        },
      );

      final response = await http.post(
        Uri.parse('${Config.URL}/forgotPassword'),
        headers: {
          'Content-Type': 'application/json'
        },
        body: jsonEncode(
          {
            "email": _userEmailController.text,
            "language": context.translate('selectLang'),
          },
        ),
      );

      if (response.statusCode == 200) {
        // Successfully sent
        Navigator.of(context, rootNavigator: true).pop();

        showDialog(
          context: context,
          builder: (BuildContext context) {
            // Future.delayed(Duration(seconds: 4), () {
            // Navigator.of(context, rootNavigator: true).pop();
            // setState(() {
            //   _userEmailController.clear();
            // });
            // });
            return SuccessfullPopup(
              title: context.translate('successUpdate'),
              subTitle: context.translate('successRegMsg'),
              onTap: () {
                Navigator.of(context).pop();
                setState(() {
                  _userEmailController.clear();
                });
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LoginScreen(loggedIn: false)),
                  (Route<dynamic> route) => false,
                );
              },
            );
          },
        );
      } else {
        setState(() {
          Navigator.of(context, rootNavigator: true).pop();
          _errorMessage = context.translate('errorEmailExisting');
        });
      }
    } catch (e) {
      setState(() {
        Navigator.of(context, rootNavigator: true).pop();
        _errorMessage = 'An error occurred please contact support';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(context.translate('forgotPassword')),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            SizedBox(height: 10),
            Text(context.translate('forgotPwdInstruction')),
            SizedBox(height: 10),
            TextField(
              controller: _userEmailController,
              decoration: DecorationComponent.inputDecoration(context).copyWith(
                labelText: context.translate('email'),
                hintText: context.translate('hintEmail'),
              ),
            ),
            ErrorText(message: _errorEmail),
            ErrorText(message: _errorMessage),
            SizedBox(height: 20.0),
            Card(
              elevation: 4,
              color: Theme.of(context).primaryColor,
              child: InkWell(
                onTap: () {
                  _submitForgotPassword(context);
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 50,
                  child: Text(
                    context.translate('resetPwd'),
                    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

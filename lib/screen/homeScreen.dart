// import 'dart:io';
import 'package:e_gumis/component/home/loginButton.dart';
import 'package:e_gumis/component/others/staticbanner.dart';
import 'package:flutter/material.dart';
import '../component/home/mainMenuButtonLanding.dart';
import '../component/home/mainMenuButtonSignedIn.dart';
import '../component/home/notifyStatusApplication.dart';
import '../component/home/settingButton.dart';
import '../component/others/announcement.dart';
import '../widget/switchLanguage.dart';

class HomeScreen extends StatefulWidget {
  final String fullname;

  HomeScreen({this.fullname = ""});
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static const textStyle = TextStyle(
      fontSize: 14, color: Colors.black, fontWeight: FontWeight.normal);

  // @override
  // void initState() {
  //   super.initState();
  //   printIps();
  // }

  // Future printIps() async {
  //   for (var interface in await NetworkInterface.list()) {
  //     for (var addr in interface.addresses) {
  //       print(
  //           '${addr.address} ${addr.host} ${addr.isLoopback} ${addr.rawAddress} ${addr.type.name}');
  //     }
  //   }

  //   DateTime now = DateTime.now();
  //   DateTime date = DateTime(now.year, now.month, now.day);

  //   print("=================================");
  //   print(now);
  //   print(date);
  // }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  widget.fullname == ""
                      ? Row(
                        children: [
                          Text('Hello,', style: textStyle),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Container(height: 30, width: 1.5, color: Colors.grey[300]),
                          ),
                          SwitchWidget(),
                        ],
                      )
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Hello,', style: textStyle),
                            Text(
                                widget.fullname.length > 30
                                    ? '${widget.fullname.substring(0, 30)}...'
                                    : widget.fullname, style: textStyle),
                          ],
                      ),
                ],
              ),
              widget.fullname == "" ? Loginbutton() : Settingbutton(),
            ],
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Top section with background image and logo
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage('assets/images/homeBG.png'),
                        ),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(100),
                          bottomRight: Radius.circular(100),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              SizedBox(height: 110),
                              Image.asset(
                                'assets/images/eGumisLogo.png',
                                width: 150,
                                height: 150,
                                fit: BoxFit.cover,
                              ),
                              SizedBox(height: 20),
                            ],
                          ),
                        ],
                      ),
                    ),
                    // Middle section with main content
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        widget.fullname == ""
                            ? SizedBox.shrink()
                            : NotifyStatus(),
                        widget.fullname == ""
                            ? MainMenuLanding()
                            : MainMenuSigneIn(),
                      ],
                    ),
                    // Announcement section
                    AnnouncementComponent(),
                  ],
                ),
              ),
            ),
            // Static banner at the bottom
            StaticBanner(),
          ],
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'package:accordion/accordion.dart';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:http/http.dart' as http;
import 'package:html/parser.dart' as html_parser;
import '../component/config.dart';
import '../component/others/loadingIndicator.dart';
import '../widget/timeTrackingState.dart';

class FaqScreen extends StatefulWidget {
  @override
  State<FaqScreen> createState() => _FaqScreenState();
}

class _FaqScreenState extends TimeTrackingState<FaqScreen> {
  static const headerStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.bold);
  // static const subtitleStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.w500);

  bool _loading = true;
  List<dynamic> faqHeaders = [];
  Map<int, List<dynamic>> faqDetails = {};
  int? _selectedHeaderId;

  final List<IconData> headerIcons = [
    Icons.radio_button_unchecked,
    Icons.edit_note,
    Icons.note,
    Icons.insert_drive_file,
    Icons.person,
    Icons.apartment,
    Icons.man,
    Icons.corporate_fare,
    Icons.help_outline,
    Icons.money,
  ];

  @override
  void initState() {
    super.initState();
    fetchFAQHeaders();

    //function for API report
    passingInfoTimeTracking('Paparan Soalan Lazim');
  }

  Future<void> fetchFAQHeaders() async {
    try {
      final response = await http.get(Uri.parse('${Config.URL}/faqHeader'));
      if (response.statusCode == 200) {
        Map<String, dynamic> jsonResponse = jsonDecode(response.body);
        setState(() {
          faqHeaders = jsonResponse['faqHeaders'] ?? [];
          faqHeaders.sort((a, b) =>
              (a['displayOrder'] ?? 0).compareTo(b['displayOrder'] ?? 0));

          if (faqHeaders.isNotEmpty) {
            _selectedHeaderId = faqHeaders.first['id'];
          }
        });
        fetchFAQDetails();
      }
    } catch (error) {
      print('Error fetching FAQ headers: $error');
      setState(() {
        _loading = false;
      });
    }
  }

  Future<void> fetchFAQDetails() async {
    try {
      final response = await http.get(Uri.parse('${Config.URL}/faqDetail'));
      if (response.statusCode == 200) {
        Map<String, dynamic> jsonResponse = jsonDecode(response.body);
        setState(() {
          for (var detail in jsonResponse['faqDetails'] ?? []) {
            int headerId = detail['faqHeaderId'] ?? 0;
            if (!faqDetails.containsKey(headerId)) {
              faqDetails[headerId] = [];
            }
            faqDetails[headerId]!.add(detail);
          }
          _loading = false;
        });
      } else {
        print('Error fetching FAQ details: Non-200 status code');
        setState(() {
          _loading = false;
        });
      }
    } catch (error) {
      print('Error fetching FAQ details: $error');
      setState(() {
        _loading = false;
      });
    }
  }

  String stripHtml(String htmlString) {
    final document = html_parser.parse(htmlString);
    String text = html_parser.parse(document.body?.text ?? '').documentElement?.text ?? '';
    
    // Replace non-breaking spaces and newline characters
    text = text.replaceAll('&nbsp;', ' ').replaceAll('\n', ' ');
    return text;
  }

  String replaceFontSize(String htmlString) {
  // Replace all font-size values with 14px
  return htmlString.replaceAllMapped(
    RegExp(r'font-size:\s?[^;]+;'),
    (match) => 'font-size: 14px;',
  );
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(context.translate('faq')),
        actions: [
          Builder(
            builder: (BuildContext context) {
              return GestureDetector(
                onTap: () {
                  Scaffold.of(context).openEndDrawer();
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.menu),
                      SizedBox(height: 2),
                      Text(
                        context.translate('category'),
                        style: TextStyle(
                          fontSize: 10,
                          color: Colors.black,
                          fontWeight: FontWeight.bold, // Bold text
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ],
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.white, Color(0xAA3a6198)],
            stops: [0.5, 1],
            begin: AlignmentDirectional(1, -0.5),
            end: AlignmentDirectional(-1, 0.5),
          ),
        ),
        child: _loading
            ? LoadingIndicator()
            : ListView(
                children: faqHeaders
                    .where((header) =>
                        _selectedHeaderId == null ||
                        header['id'] == _selectedHeaderId)
                    .map<Widget>((header) {
                  List<AccordionSection> sections = faqDetails[header['id']]
                          ?.asMap()
                          .entries
                          .map<AccordionSection>((entry) {
                        int index = entry.key + 1; // 1-based index
                        var detail = entry.value;
                        return AccordionSection(
                          isOpen: true,
                          header: Text(
                              '$index. ${stripHtml(detail[context.translate('questionBody')] ?? '')}',
                              style: headerStyle),
                          content: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            // child: Text(
                            //   stripHtml(
                            //       detail[context.translate('answerBody')] ?? ''),
                            //   style: subtitleStyle,
                            //   textAlign: TextAlign.justify, // Justify text
                            // ),
                            child: Html(data: replaceFontSize(detail[context.translate('answerBody')]),
                            ),
                          ),
                        );
                      }).toList() ??
                      [];

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 16, horizontal: 20),
                        child: Text(
                          header[context.translate('titleUrl')] ?? '',
                          style: headerStyle,
                        ),
                      ),
                      Accordion(
                        rightIcon: Icon(Icons.keyboard_arrow_down,
                            color: Colors.black),
                        disableScrolling: true,
                        headerBorderWidth: 1,
                        headerBorderColor: Theme.of(context).primaryColor,
                        headerBackgroundColor: Colors.white,
                        contentBorderColor: Theme.of(context).primaryColor,
                        contentBorderWidth: 1,
                        contentHorizontalPadding: 20,
                        contentVerticalPadding: 20,
                        scaleWhenAnimating: true,
                        openAndCloseAnimation: true,
                        headerPadding: const EdgeInsets.symmetric(
                            vertical: 7, horizontal: 15),
                        children: sections,
                      ),
                    ],
                  );
                }).toList(),
              ),
      ),
      endDrawer: Drawer(
        backgroundColor: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            children: <Widget>[
              Container(
                height: 70,
                child: DrawerHeader(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        context.translate('faqCategories'),
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.all(2.0),
                  padding: EdgeInsets.all(2.0),
                ),
              ),
              Expanded(
                child: ListView(
                  children: faqHeaders.asMap().entries.map((entry) {
                    int index = entry.key;
                    var faq = entry.value;
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 13.0),
                      child: ListTile(
                        leading: Icon(
                          headerIcons[index % headerIcons.length],
                          color: _selectedHeaderId == faq['id']
                              ? Colors.white
                              : Colors.black,
                        ),
                        title: Text(
                          faq[context.translate('titleUrl')] ?? 'No Title',
                          style: TextStyle(
                            color: _selectedHeaderId == faq['id']
                                ? Colors.white
                                : Colors.black,
                          ),
                        ),
                        tileColor: _selectedHeaderId == faq['id']
                            ? Theme.of(context).primaryColor
                            : Colors.transparent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                        onTap: () {
                          setState(() {
                            _selectedHeaderId = faq['id'];
                          });
                          Navigator.pop(context);
                        },
                      ),
                    );
                  }).toList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
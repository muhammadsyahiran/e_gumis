import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:accordion/accordion.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../component/config.dart';
import '../component/others/loadingIndicator.dart';
import 'package:e_gumis/I10n/localizations.dart';
import '../component/wtdListApproval/cardComponent.dart';
import '../widget/timeTrackingState.dart';

class WTDlistApprovaleClaimInfoScreen extends StatefulWidget {
  late final int rfdInfoId;  
  WTDlistApprovaleClaimInfoScreen({required this.rfdInfoId});

  @override
  State<WTDlistApprovaleClaimInfoScreen> createState() => _WTDlistApprovaleClaimInfoScreenState();
}

class _WTDlistApprovaleClaimInfoScreenState extends TimeTrackingState<WTDlistApprovaleClaimInfoScreen> {
  List<dynamic> statusHistory = [];
  List<dynamic> beneficialOwners = [];
  Map<String, dynamic>? payeeInfo;
  Map<String, dynamic>? refundInformation;
  Map<String, String> stateMap = {};
  Map<String, String> countryMap = {};
  bool loading = true;

  @override
  void initState() {
    super.initState();
    fetchData();
    
    //function for API report
    passingInfoTimeTracking('Paparan Maklumat Status WTD');
  }

  Future<void> fetchData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? tokenValue = prefs.getString('tokenValue');

    final response = await http.get(Uri.parse('${Config.URL}/wtdStatusDetails/${widget.rfdInfoId}'),
      headers: {
        'Authorization': 'Bearer ${tokenValue}',
        'Content-Type': 'application/json',
      },
    );

    final responseCountry = await http.get(Uri.parse('${Config.URL}/country'));
      Map<String, dynamic> responseCountryApi = json.decode(responseCountry.body);
      List<dynamic> countriesApi = responseCountryApi['countries'];
      setState(() {
        countryMap = {
          for (var country in countriesApi) country['code']: country['nameEn']
        };
      });
      // print(countriesApi);

    final responseState = await http.get(Uri.parse('${Config.URL}/state'));
      Map<String, dynamic> responseStateApi = json.decode(responseState.body);
      List<dynamic> statesApi = responseStateApi['states'];
      setState(() {
        stateMap = {
          for (var state in statesApi) state['code']: state['nameEn']
        };
      });
      // print(statesApi);

    Map<String, dynamic> responseApi = jsonDecode(response.body);
    final statusHistoryApi = responseApi['statusHistory'] ?? [];
    final beneficialOwnersApi = responseApi['beneficialOwners'] ?? [];
    final payeeInfolApi = responseApi['payeeInfo'] ?? [];
    final refundInformationApi = responseApi['refundInformation'] ?? [];

    // Sort statusHistory based on statusDate in ascending order (old to new)
    statusHistoryApi.sort((a, b) {
      DateTime dateA = DateTime.parse(a['statusDate']);
      DateTime dateB = DateTime.parse(b['statusDate']);
      return dateB.compareTo(dateA);  // Ascending order
    });

    // Format the statusDate as DD-MM-YYYY
    for (var item in statusHistoryApi) {
      item['formattedStatusDate'] = formatDate(item['statusDate']);
    }
    
    if (response.statusCode == 200) {
      setState(() {
        statusHistory = statusHistoryApi;
        beneficialOwners = beneficialOwnersApi;
        payeeInfo = payeeInfolApi;
        refundInformation = refundInformationApi;
        loading = false;
      });
    } else {
      setState(() {
        loading = false;
      });
      throw Exception('Failed to load data');
    }
  }
  // Function to format date as DD-MM-YYYY
  String formatDate(String dateString) {
    DateTime parsedDate = DateTime.parse(dateString);
    return DateFormat('dd-MM-yyyy').format(parsedDate);
  }

  static const headerStyle = TextStyle(fontSize: 15, fontWeight: FontWeight.bold);
  static const subHeaderStyle = TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white);
  static const titleStyle = TextStyle(fontSize: 12, color: Color(0xffee8b60));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(context.translate('refundInformation'))),
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.white, Color(0xAA3a6198)],
            stops: [0.5, 1],
            begin: AlignmentDirectional(1, -0.5),
            end: AlignmentDirectional(-1, 0.5),
          ),
        ),
        child: loading
            ? LoadingIndicator()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Accordion(
                      disableScrolling: true,
                      leftIcon: Icon(Icons.format_list_bulleted),
                      rightIcon: Icon(Icons.keyboard_arrow_down),
                      headerBorderWidth: 1.5,
                      headerBorderColor: Theme.of(context).primaryColor,
                      headerBackgroundColor: Colors.white,
                      contentBorderColor: Theme.of(context).primaryColor,
                      contentBorderWidth: 1.5,
                      contentHorizontalPadding: 20,
                      contentVerticalPadding: 20,
                      scaleWhenAnimating: true,
                      openAndCloseAnimation: true,
                      headerPadding: const EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                      children: [
                        AccordionSection(
                          // isOpen: true,
                          header: Text(context.translate('claimantDetails'), style: headerStyle),
                          content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(context.translate('fullName'), style: titleStyle),
                              Text(refundInformation!['claimantName'] ?? ''),
                              SizedBox(height: 10),

                              Text(context.translate('idNumber'), style: titleStyle),
                              Text(refundInformation!['identificationNo'] ?? ''),
                              SizedBox(height: 10),
                              
                              Text('Telefon', style: titleStyle),
                              Text(refundInformation!['phoneNo'] ?? ''),
                              SizedBox(height: 10),

                              Text(context.translate('numberMobile'), style: titleStyle),
                              Text(refundInformation!['mobileNo'] ?? ''),
                              SizedBox(height: 10),

                              Text(context.translate('fax'), style: titleStyle),
                              Text(refundInformation!['faxNo'] ?? ''),
                              SizedBox(height: 10),
                              
                              Text(context.translate('email'), style: titleStyle),
                              Text(refundInformation!['email'] ?? ''),
                              SizedBox(height: 10),
                              
                              Text(context.translate('address'), style: titleStyle),
                              Text(refundInformation!['address1'] ?? ''),
                              Text(refundInformation!['address2'] ?? ''),
                              Text('${refundInformation!['postcode'] ?? ''} ${refundInformation!['city'] ?? ''}'),
                              Text(stateMap[refundInformation!['state']] ?? ''),
                              Text(countryMap[refundInformation!['country']] ?? ''),
                              SizedBox(height: 10),
                            ],
                          ),
                        ),
                        AccordionSection(
                          // isOpen: true,
                          header: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(context.translate('beneficialOwner'), style: headerStyle),
                              if (statusHistory.any((item) => item['status'] == "11"))
                              Container(
                                  color: Color(0xff249689),
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 5, right: 5),
                                    child:  Text(
                                      context.translate('paidStatus'),
                                      style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                                    ),
                                  ))
                            ],
                          ),
                          content: Accordion(
                            disableScrolling: true,
                            paddingListTop: 0,
                            paddingListBottom: 0,
                            maxOpenSections: 1,
                            headerBackgroundColor: Color(0xff57636c),
                            headerBackgroundColorOpened: Color(0xff57636c),
                            headerPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 7),
                            children: beneficialOwners.map((item) {
                              return AccordionSection(
                                isOpen: true,
                                header: Text('${context.translate('beneficialOwner')} ${beneficialOwners.indexOf(item) + 1}', style: subHeaderStyle),
                                content: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(context.translate('name'), style: titleStyle),
                                    Text(item['name'] ?? ''),
                                    SizedBox(height: 10),

                                    Text(context.translate('idNumber'), style: titleStyle),
                                    Text(item['newIcNumber'] ?? ''),
                                    SizedBox(height: 10),

                                    Text(context.translate('refNumber'), style: titleStyle),
                                    Text(item['otherRefNo'] ?? ''),
                                    SizedBox(height: 10),
                                   
                                    Text(context.translate('wtdType'), style: titleStyle),
                                    Text(item['wtdTypeNameMs'] ?? ''),
                                    SizedBox(height: 10),
                                    
                                    Text(context.translate('financeYear'), style: titleStyle),
                                    Text(item['financialYear'] ?? ''),
                                    SizedBox(height: 10),
                                    
                                    Text(context.translate('entityName'), style: titleStyle),
                                    Text(item['entityName'] ?? ''),
                                    SizedBox(height: 10),

                                    Text(context.translate('amountRM'), style: titleStyle),
                                    Text(item['claimAmount']?.toStringAsFixed(2) ??''),
                                    SizedBox(height: 10),

                                    // Text("${context.translate('amount')} (RM)",style: titleStyle),
                                    // Text(item['unclaimed_Amount']?.toStringAsFixed(2) ?? ''),
                                    // SizedBox(height: 10),
                                  ],
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                        AccordionSection(
                          // isOpen: true,
                          header: Text(context.translate('payeeDetails'), style: headerStyle),
                          content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(context.translate('fullName'), style: titleStyle),
                              Text(payeeInfo!['name'] ?? ''),
                              SizedBox(height: 10),

                              Text(context.translate('idNumber'), style: titleStyle),
                              Text(payeeInfo!['idNo'] ?? ''),
                              SizedBox(height: 10),

                              Text(context.translate('swiftCode'), style: titleStyle),
                              Text(payeeInfo!['bankCode'] ?? ''),
                              SizedBox(height: 10),

                              Text(context.translate('accNo'), style: titleStyle),
                              Text(payeeInfo!['accNo'] ?? ''),
                              SizedBox(height: 10),

                              Text(context.translate('numberMobile'), style: titleStyle),
                              Text(payeeInfo!['mobileNo'] ?? ''),
                              SizedBox(height: 10),

                              Text('Telefon', style: titleStyle),
                              Text(payeeInfo!['phoneNo'] ?? ''),
                              SizedBox(height: 10),

                              Text(context.translate('fax'), style: titleStyle),
                              Text(payeeInfo!['faxNo'] ?? ''),
                              SizedBox(height: 10),

                              Text(context.translate('email'), style: titleStyle),
                              Text(payeeInfo!['email'] ?? ''),
                              SizedBox(height: 10),
                              
                              Text(context.translate('address'), style: titleStyle),
                              Container(
                                  width: 200,
                                  child: Text(payeeInfo!['address'] ?? '')),
                              Text('${payeeInfo!['postcode'] ?? ''} ${payeeInfo!['city'] ?? ''}'),
                              Text(countryMap[payeeInfo!['country']] ?? ''),
                              SizedBox(height: 10),
                              
                              Text(context.translate('amount'), style: titleStyle),
                              Text(payeeInfo!['claimAmount']?.toStringAsFixed(2) ?? ''),
                            ],
                          ),
                        ),
                        AccordionSection(
                          isOpen: true,
                          header: Text(context.translate('claimStatus'), style: headerStyle),
                          content: Accordion(
                              disableScrolling: true,
                              paddingListTop: 0,
                              paddingListBottom: 0,
                              maxOpenSections: 1,
                              headerBackgroundColor: Color(0xff57636c),
                              headerBackgroundColorOpened: Color(0xff57636c),
                              headerPadding: EdgeInsets.symmetric(vertical: 7, horizontal: 7),
                              children: statusHistory.map((item) {
                                return AccordionSection(
                                  isOpen: true,
                                  header: Text(StatusUtil.getStatusText(context, item['status'] ?? ''), style: subHeaderStyle),
                                  content: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(context.translate('refNoQuery'), style: titleStyle),
                                      Text(item['fileRefNo'] ?? ''),
                                      SizedBox(height: 10),

                                      Text(context.translate('date'), style: titleStyle),
                                      Text(item['formattedStatusDate'] ?? ''),
                                      SizedBox(height: 10),
                                      
                                      Text(context.translate('description'), style: titleStyle),
                                      Text(item['statusDescription'] ?? ''),
                                      SizedBox(height: 10),
                                    ],
                                  ),
                                );
                              }).toList()),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}

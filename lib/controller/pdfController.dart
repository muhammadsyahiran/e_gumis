import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart' as path;
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OpenPdfUtil {
  Future<File> loadPdfFromNetwork(String url) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? tokenValue = prefs.getString('tokenValue');

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Authorization': 'Bearer $tokenValue',
        'Content-Type': 'application/json',
      });

      print(response.headers);

      if (response.statusCode == 200 &&
          response.headers['content-type']!.contains('application/pdf')) {
        final bytes = response.bodyBytes;
        return _storeFile(url, bytes);
      } else {
        throw Exception('File is not a PDF or the request failed');
      }
    } catch (e) {
      print('Error loading PDF: $e');
      rethrow;
    }
  }

  Future<File> _storeFile(String url, List<int> bytes) async {
    // Ensure the file has a .pdf extension
    final filename = path.basename(url);
    final String pdfFilename =
        filename.endsWith(".pdf") ? filename : "$filename.pdf";

    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/$pdfFilename');
    await file.writeAsBytes(bytes, flush: true);
    if (kDebugMode) {
      print('File saved at: $file');
    }
    return file;
  }
}

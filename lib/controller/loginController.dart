import 'dart:async';
import 'dart:convert';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../component/others/loadingIndicator.dart';
import '../component/config.dart';
import '../screen/profileScreen.dart';
import '../screen/resetPasswordScreen.dart';
import '../screen/homeScreen.dart';
import '../service/apiReport.dart';
import '../widget/clickTrackingState.dart';
import '../widget/globalLogOutPopup.dart';

class LoginController {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool obscureText = true;
  String errorMessage = '';
  String errorUserName = '';
  String errorPassword = '';
  bool passwordIncorrect = false;
  bool accountLock = false;
  int remainingAttempts = 3;

  void togglePasswordVisibility(Function updateState) {
    updateState(() {
      obscureText = !obscureText;
    });
  }

  Future<void> loginAPI(BuildContext context, Function updateState) async {
    updateState(() {
      showDialog(
        context: context,
        builder: (context) {
          return LoadingIndicator();
        },
      );
    });

    String username = usernameController.text;
    String password = passwordController.text;
    // String username = "eijamuhd";
    // String username = "Xiaosky96";
    // String username = "zyothman";
    // String username = "yapbg";
    // String username = "muhammadhariz";
    // String username = "Sabriaiman";
    // String username = "Azleza7778";
    // String username = "Lieza3656";
    // String username = "123_4";
    // String username = "hushela";
    // String username = "Sabriaiman";
    // String username = "fullyfully";
    // String password = "P@ssw0rd";
    // String password = "P@ssw0rd1234";
    // String password = "Azliza`1";
    // String password = "Azliza1@@";

    updateState(() {
      errorUserName = '';
      errorPassword = '';
      errorMessage = '';
    });

    if (username.isEmpty && password.isEmpty) {
      updateState(() {
        Navigator.of(context, rootNavigator: true).pop();
        errorUserName = context.translate('userNameError');
        errorPassword = context.translate('passwordError');
      });
      return;
    }

    if (username.isEmpty) {
      updateState(() {
        Navigator.of(context, rootNavigator: true).pop();
        errorUserName = context.translate('userNameError');
      });
      return;
    }
    if (password.isEmpty) {
      updateState(() {
        Navigator.of(context, rootNavigator: true).pop();
        errorPassword = context.translate('passwordError');
      });
      return;
    }

    try {
      final responseLogin = await http.post(
        Uri.parse('${Config.URL}/api/login'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          'username': username,
          'password': password,
        }),
      );

      final data = jsonDecode(responseLogin.body);

      if (responseLogin.statusCode == 200) {
        final responseProfile = await http.get(
          Uri.parse('${Config.URL}/profile/${data['username']}'),
          headers: {
            'Authorization': 'Bearer ${data['access_token']}',
            'Content-Type': 'application/json',
          },
        );

        Map<String, dynamic> responseApi = jsonDecode(responseProfile.body);
        final user = responseApi['userProfile'];

        await setLocalStorage(data, user);

        if (user['firstLogin'] == true || user['passwordExpired'] == true) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => ResetPwdScreen(toForceProfile: true, isPasswordExpired: user['passwordExpired'])),
            (Route<dynamic> route) => false,
          );

          //function for API report
          trackClick('Paparan Kemas Kini Kata Laluan');
        } else if (user['profileUpdated'] == false) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => ProfileScreen(toForceProfile: true)),
            (Route<dynamic> route) => false,
          );
        } else {
          updateState(() {
            Navigator.of(context, rootNavigator: true).pop();
          });
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => HomeScreen(
                    // username: data['username'],
                    fullname: user['fullName'])),
            (Route<dynamic> route) => false,
          );
        }

        triggerLogOutPopup(context);
      } else {
        handleLoginErrors(responseLogin, context, updateState);
        if (data['code'] == 'ERR_LOGIN_BAD_CREDENTIALS') {
          //function for API report
          ApiService().reportFailLogin(username);
        }
      }
    } catch (e) {
      updateState(() {
        errorMessage = 'No Internet';
        Navigator.of(context, rootNavigator: true).pop();
      });
    }
  }

  Future<void> setLocalStorage(data, user) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('tokenValue', data['access_token']);
    await prefs.setString('usernameValue', data['username']);
    await prefs.setString('fullnameValue', user['fullName']);
    await prefs.setString('identityNumberValue', user['identityNumber']);
  }

  void handleLoginErrors(http.Response responseLogin, BuildContext context, Function updateState) {
    final data = jsonDecode(responseLogin.body);
    updateState(() {
      if (data['code'] == 'ERR_LOGIN_FAIL') {
        errorMessage = context.translate('errorMsg');
      } else if (data['code'] == 'ERR_LOGIN_BAD_CREDENTIALS') {
        usernameController.clear();
        passwordController.clear();
        errorMessage = context.translate('errorMsg');
        passwordIncorrect = true;
        remainingAttempts = 4 - (data['failedAttempt'] as int);
      } else if (data['code'] == 'ERR_LOGIN_ACCOUNT_LOCKED') {
        usernameController.clear();
        passwordController.clear();
        passwordIncorrect = false;
        accountLock = true;
      }
      Navigator.of(context, rootNavigator: true).pop();
    });
  }
}

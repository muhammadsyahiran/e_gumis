import 'package:e_gumis/I10n/localizations.dart';
import 'package:flutter/material.dart';

class VersionNumber extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '${context.translate('copyRight')} 2024 \n© Jabatan Akauntan Negara Malaysia \n${context.translate('version')} 21.0.0',
      textAlign: TextAlign.center,
      style: TextStyle(color: Theme.of(context).primaryColor),
    );
  }
}

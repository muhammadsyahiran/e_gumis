import 'package:e_gumis/I10n/localizations.dart';
import 'package:e_gumis/widget/clickTrackingState.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../main.dart';
import '../screen/settingScreen.dart';

class SwitchWidget extends StatefulWidget {
  @override
  State<SwitchWidget> createState() => _SwitchWidgetState();
}

class _SwitchWidgetState extends State<SwitchWidget> {
  @override
  Widget build(BuildContext context) {

  //function for API report
  trackClick('Konfigurasi Tukar Bahasa');  
  
  var fontStyle = TextStyle(fontSize: 14);
  var languageProvider = Provider.of<LanguageProvider>(context);
  return Row(
    children: [
      Text('BM',style: fontStyle),
      Switch(
        value: languageProvider.locale.languageCode == 'en',
        onChanged: (value) {
          setState(() {
            var newLocale =
                value ? Locale('en', '') : Locale('ms', '');
            languageProvider.setLocale(newLocale);
            MyApp.setLocale(context, newLocale);
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content:
                    Text(context.translate('changeLanguage')),
              ),
            );
          });
        },
        activeColor: Colors.black,
        inactiveThumbColor: Colors.amber,
      ),
      Text('EN',style: fontStyle,),
    ],
  );
  }
}
import 'dart:async';
import 'package:e_gumis/I10n/localizations.dart';
import 'package:e_gumis/screen/loginScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../main.dart';
import '../component/wtdListApproval/cardComponent.dart';

Future<void> triggerLogOutPopup(BuildContext context) async {
  String localizedTitle = context.translate('logOut');

  Timer(const Duration(minutes: 5), () {
    showTimerLogOutPopup(context, title: localizedTitle);
  });
}

void showTimerLogOutPopup(
  BuildContext context, {
  required String title,
}) {
  showDialog(
    context: navigatorKey.currentContext!,
    builder: (BuildContext context) {
      _cancelBtn() {
        Navigator.of(context).pop(true);
        triggerLogOutPopup(context);
      }

      _logOutBtn() async {
        final SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.remove('tokenValue');
        await prefs.remove('fullNameValue');
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => LoginScreen(
                      loggedIn: false,
                    )),
            (Route<dynamic> route) => false);
      }

      return Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Card(
              child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        title,
                        style: TextStyle(
                          fontSize: 21,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(context.translate('logOutMsgtimer')),
                      // Text("Tempoh penggunaan applikasi eGUMIS tamat, \nsila klik 'Tidak' untuk teruskan penggunaan"),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {
                              _cancelBtn();
                            },
                            child: ButtonComponent(textBtn: context.translate('logOutBtnNo'), colorBtn: Theme.of(context).primaryColor),
                          ),
                          InkWell(
                            onTap: () {
                              _logOutBtn();
                            },
                            child: ButtonComponent(
                              textBtn: context.translate('logOut'),
                              colorBtn: Color.fromARGB(255, 255, 125, 138),
                            ),
                          )
                        ],
                      )
                    ],
                  )),
            ),
          ],
        ),
      );
    },
  );
}

import 'package:e_gumis/component/config.dart';
import 'package:e_gumis/service/apiReport.dart';
import 'package:shared_preferences/shared_preferences.dart';


  Future<void> trackClick(title) async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String identityNumberValue = prefs.getString('identityNumberValue') ?? "Pelawat";
    ApiService().reportTotalClicks(title, await getPublicIpAddress(), identityNumberValue);
  }


import 'package:e_gumis/service/apiReport.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class TimeTrackingState<T extends StatefulWidget> extends State<T> {
  DateTime? _startTime;
  DateTime? _endTime;
  String? screenTitle;

  void passingInfoTimeTracking(title) {
    screenTitle = title;
  }

  @override
  void initState() {
    super.initState();
    _startTime = DateTime.now();
  }

  @override
  void dispose() async {
    _endTime = DateTime.now();

    SharedPreferences.getInstance().then((prefs) {
      final String identityNumberValue = prefs.getString('identityNumberValue') ?? "Pelawat";
      ApiService().reportAppUsage(screenTitle, identityNumberValue, _startTime.toString(), _endTime.toString());
    });

    super.dispose();
  }
}

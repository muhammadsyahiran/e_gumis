import 'package:e_gumis/screen/settingScreen.dart';
import 'package:e_gumis/screen/splashScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'I10n/localizations.dart';
// import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatefulWidget {
  static void setLocale(BuildContext context, Locale newLocale) {
    final state = context.findAncestorStateOfType<_MyAppState>();
    state?.setLocale(newLocale);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale = Locale('ms', ''); // Default locale is Malay

  void setLocale(Locale newLocale) {
    setState(() {
      _locale = newLocale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => LanguageProvider(),
      child: MaterialApp(
        navigatorKey: navigatorKey, // Set global navigator key
        debugShowCheckedModeBanner: false,
        title: 'eGumis',
        theme: ThemeData(
            useMaterial3: false,
            // brightness: Brightness.dark,
            canvasColor: Colors.white,
            primaryColor: Color(0xFF003478),
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
            scaffoldBackgroundColor: Colors.grey[100],
            fontFamily: 'Plus Jakarta Sans',
            appBarTheme: AppBarTheme(
              systemOverlayStyle: SystemUiOverlayStyle(
                statusBarColor: Colors.white, // For background color
                statusBarIconBrightness: Brightness.dark, // For Android
                statusBarBrightness: Brightness.light, // For iOS
              ),
              backgroundColor: Colors.transparent,
              elevation: 0,
              iconTheme: IconThemeData(color: Color(0xFF003478)),
              titleTextStyle: TextStyle(fontFamily: 'Plus Jakarta Sans', color: Color(0xAA003478), fontSize: 20, fontWeight: FontWeight.bold),
            ),
            cardTheme: CardTheme(
              color: Colors.white,
              elevation: 4,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            )),
        localizationsDelegates: [
          const AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', ''), // English
          const Locale('ms', ''), // Malay
        ],
        locale: _locale,
        home: SplashScreen(),
      ),
    );
  }
}

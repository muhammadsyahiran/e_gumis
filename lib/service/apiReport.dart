import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../component/config.dart';

class ApiService {
  Future<void> reportWtdTotalSearchPublic(String searchValue) async {
    final reportWtdTotalSearchPublic = await http.post(
      Uri.parse('${Config.URL}/reportWtdTotalSearch'),
      headers: {
        'Content-Type': 'application/json'
      },
      body: jsonEncode(
        {
          "username": 'Pelawat/Visitor',
          "fullName": 'Pelawat/Visitor',
          "identityNumber": 'Pelawat/Visitor',
          "searchValue": searchValue,
          "ipAddress": await getPublicIpAddress()
        },
      ),
    );
    print(reportWtdTotalSearchPublic.body);
  }

  Future<void> reportWtdTotalSearch(String searchValue) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? userNameValue = prefs.getString('usernameValue');
    final String? fullNameValue = prefs.getString('fullnameValue');
    final String? identityNumberValue = prefs.getString('identityNumberValue');

    final reportWtdTotalSearch = await http.post(
      Uri.parse('${Config.URL}/reportWtdTotalSearch'),
      headers: {
        'Content-Type': 'application/json'
      },
      body: jsonEncode(
        {
          "username": userNameValue ?? '',
          "fullName": fullNameValue ?? '',
          "identityNumber": identityNumberValue ?? '',
          "searchValue": searchValue,
          "ipAddress": await getPublicIpAddress()
        },
      ),
    );
    print(reportWtdTotalSearch.body);
  }

  Future<void> reportFailLogin(String usernameValue) async {
    print(usernameValue);
    final reportFailLogin = await http.post(
      Uri.parse('${Config.URL}/reportFailLogin'),
      headers: {
        'Content-Type': 'application/json'
      },
      body: jsonEncode(
        {
          "username": usernameValue,
          "failLoginAttempt": "1"
        },
      ),
    );
    print(reportFailLogin.body);
  }

  Future<void> reportResetPassword(String username, String fullname, String identityNumber) async {
    final reportResetPassword = await http.post(
      Uri.parse('${Config.URL}/reportResetPassword'),
      headers: {
        'Content-Type': 'application/json'
      },
      body: jsonEncode(
        {
          "username": username,
          "fullName": fullname,
          "identityNumber": identityNumber
        },
      ),
    );
    print(reportResetPassword.body);
  }

  Future<void> reportUserRegistration(username, fullname, identityNumber) async {
    final reportUserRegistration = await http.post(
      Uri.parse('${Config.URL}/reportUserRegistration'),
      headers: {
        'Content-Type': 'application/json'
      },
      body: jsonEncode(
        {
          "username": username,
          "fullName": fullname,
          "identityNumber": identityNumber
        },
      ),
    );
    print(reportUserRegistration.body);
  }

  Future<void> reportAppUsage(moduleTitle, identityNumber, startDate, endDate) async {
    final reportAppUsage = await http.post(
      Uri.parse('${Config.URL}/reportAppUsage'),
      headers: {
        'Content-Type': 'application/json'
      },
      body: jsonEncode(
        {
          "module": moduleTitle, //"Carian WTD",
          "identityNumber": identityNumber, //"444444444444",
          "startDate": startDate, //"2024-11-11 10:42:46.657",
          "endDate": endDate, //"2024-11-11 10:59:46.657"
        },
      ),
    );
    print(reportAppUsage.body);
  }

  Future<void> reportTotalClicks(moduleTitle, userIPaddress, identityNumber) async {
    final reportTotalClicks = await http.post(
      Uri.parse('${Config.URL}/reportTotalClicks'),
      headers: {
        'Content-Type': 'application/json'
      },
      body: jsonEncode(
        {
          "module": moduleTitle,
          "ipAddress": userIPaddress,
          "identityNumber": identityNumber,
          "totalClicks": "1"
        },
      ),
    );
    print(reportTotalClicks.body);
  }
}